package com.hoqhuuep.islandcraft.worldguard;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.hoqhuuep.islandcraft.Island;
import com.hoqhuuep.islandcraft.bukkit.IslandEvent;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.World;
import com.hoqhuuep.minecraft.bukkit.BukkitFactory;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.databases.ProtectionDatabaseException;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class IslandListener implements Listener {
    private final WorldGuardPlugin worldGuard;

    public IslandListener(final WorldGuardPlugin worldGuard) {
        this.worldGuard = worldGuard;
    }

    /**
     * Called whenever an island is updated. For example if it is loaded, purchased or abandoned.
     * 
     * @param event
     */
    @EventHandler
    public void onIsland(final IslandEvent event) {
        final Island island = event.getIsland();
        final Location location = island.getLocation();
        final World world = location.getWorld();
        final org.bukkit.World bukkitWorld = BukkitFactory.toBukkitWorld(world);
        final RegionManager regionManager = worldGuard.getRegionManager(bukkitWorld);
        if (regionManager == null) {
            return;
        }
        final String ownerName;
        final OfflinePlayer owner = island.getOwner();
        if (owner == null) {
            ownerName = null;
        } else {
            ownerName = owner.getName();
        }
        final int x = location.getX();
        final int z = location.getZ();
        final int radius = event.getRealEstateManager().getIslandCraftWorld(world).getOuterRadius();
        final BlockVector min = new BlockVector(x - radius, 0, z - radius);
        final BlockVector max = new BlockVector(x + radius - 1, bukkitWorld.getMaxHeight() - 1, z + radius - 1);
        final String id = "ic'" + x + "'" + z;
        createProtectedRegion(regionManager, min, max, ownerName, id);
    }

    private ProtectedRegion createProtectedRegion(final RegionManager regionManager, final BlockVector min, final BlockVector max, final String owner, final String id) {
        // Check if region already exists
        final ProtectedRegion existingRegion = regionManager.getRegion(id);
        if (existingRegion != null) {
            final DefaultDomain owners = existingRegion.getOwners();
            if (owner == null && owners.size() == 0) {
                return existingRegion;
            }
            if (owner != null && owners.size() == 1 && owners.contains(owner)) {
                return existingRegion;
            }
        }
        final ProtectedRegion newRegion = new ProtectedCuboidRegion(id, min, max);
        if (owner == null) {
            newRegion.setFlag(DefaultFlag.BUILD, State.DENY);
            addProtectedRegion(regionManager, newRegion);
            return newRegion;
        }
        final DefaultDomain owners = new DefaultDomain();
        owners.addPlayer(owner);
        newRegion.setOwners(owners);
        addProtectedRegion(regionManager, newRegion);
        return newRegion;
    }

    private void addProtectedRegion(final RegionManager regionManager, final ProtectedRegion protectedRegion) {
        regionManager.addRegion(protectedRegion);
        try {
            regionManager.save();
        } catch (final ProtectionDatabaseException e1) {
            // Try twice more
            try {
                regionManager.save();
            } catch (final ProtectionDatabaseException e2) {
                // Try once more
                try {
                    regionManager.save();
                } catch (final ProtectionDatabaseException e3) {
                    // Oh well...
                }
            }
        }
    }
}
