package com.hoqhuuep.minecraft.bukkit.nms.v1_6_R1;

import org.bukkit.World;
import org.bukkit.craftbukkit.v1_6_R1.CraftWorld;

import net.minecraft.server.v1_6_R1.WorldProvider;

import com.hoqhuuep.minecraft.bukkit.nms.BiomeGenerator;
import com.hoqhuuep.minecraft.bukkit.nms.NmsWrapper;

public class NmsHandler extends NmsWrapper {
    @Override
    public boolean installBiomeGenerator(final World world, final BiomeGenerator biomeGenerator) {
        if (!(world instanceof CraftWorld)) {
            // Wrong version?
            return false;
        }
        final CraftWorld craftWorld = (CraftWorld) world;
        final WorldProvider worldProvider = craftWorld.getHandle().worldProvider;
        if (worldProvider.e instanceof CustomWorldChunkManager) {
            // Already installed
            return false;
        }
        worldProvider.e = new CustomWorldChunkManager(world, biomeGenerator);
        return true;
    }
}
