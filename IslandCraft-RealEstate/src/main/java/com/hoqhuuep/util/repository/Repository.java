package com.hoqhuuep.util.repository;

import java.util.List;

import com.google.common.collect.Lists;
import com.hoqhuuep.util.property.Property;

public abstract class Repository<T> {
    public abstract void save(T thing);

    public abstract List<T> loadAll();

    // Can be overridden for speed
    public List<T> load(final Property<T> property) {
        final List<T> all = loadAll();
        final List<T> result = Lists.newArrayList();
        for (final T thing : all) {
            if (property.isSatisfiedBy(thing)) {
                result.add(thing);
            }
        }
        return result;
    }
}
