package com.hoqhuuep.util.repository.ebean;

import java.util.List;

import com.avaje.ebean.EbeanServer;
import com.google.common.collect.Lists;
import com.hoqhuuep.util.repository.Repository;

public abstract class EbeanRepository<T, B> extends Repository<T> {
    protected final EbeanServer ebean;
    private final Class<B> clazz;

    protected EbeanRepository(final EbeanServer ebean, final Class<B> clazz) {
        this.ebean = ebean;
        this.clazz = clazz;
    }

    public abstract B toBean(T thing);

    public abstract T fromBean(B bean);

    public abstract Object getId(B bean);

    @Override
    public void save(final T thing) {
        final B bean = toBean(thing);
        ebean.save(bean);
    }

    @Override
    public final List<T> loadAll() {
        final List<B> beans = ebean.find(clazz).findList();
        return beansToThings(beans);
    }

    protected final List<T> beansToThings(final List<B> beans) {
        final List<T> things = Lists.newArrayListWithCapacity(beans.size());
        for (final B bean : beans) {
            final T thing = fromBean(bean);
            things.add(thing);
        }
        return things;
    }
}
