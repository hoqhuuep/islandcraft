package com.hoqhuuep.util.locale;

import java.io.File;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

public final class ResourceBundleLocalizer {
    private static class PhraseInvocationHandler implements InvocationHandler {
        private final ResourceBundle bundle;

        private PhraseInvocationHandler(final ResourceBundle bundle) {
            this.bundle = bundle;
        }

        @Override
        public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
            return format(method.getName(), args);
        }

        private String format(final String phrase, final Object... args) {
            final String pattern = bundle.getString(phrase);
            if (pattern == null) {
                return formatUnknown(phrase, args);
            }
            final MessageFormat messageFormat = new MessageFormat(pattern, bundle.getLocale());
            return messageFormat.format(args, new StringBuffer(), null).toString();
        }

        private String formatUnknown(final String phrase, final Object... args) {
            return phrase + "(" + StringUtils.join(args, ", ") + ")";
        }
    }

    public static <T> T getPhraseBundle(final Class<T> clazz, final File directory) {
        try {
            final URL[] urls = { directory.toURI().toURL() };
            final ClassLoader loader = new URLClassLoader(urls);
            final ResourceBundle bundle = ResourceBundle.getBundle("messages", Locale.getDefault(), loader);
            return getPhraseBundle(clazz, bundle);
        } catch (final MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T getPhraseBundle(final Class<T> clazz, final ResourceBundle bundle) {
        return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class<?>[] { clazz }, new PhraseInvocationHandler(bundle));
    }
}
