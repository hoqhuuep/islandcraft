package com.hoqhuuep.util.property;

public abstract class Property<T> {
    public abstract boolean isSatisfiedBy(T island);

    public final Property<T> and(final Property<T> that) {
        return new ConjunctProperty<T>(this, that);
    }

    public final Property<T> or(final Property<T> that) {
        return new DisjunctProperty<T>(this, that);
    }

    public final Property<T> not() {
        return new NegativeProperty<T>(this);
    }
}
