package com.hoqhuuep.util.property;

final class ConjunctProperty<T> extends Property<T> {
    private final Property<T> a;
    private final Property<T> b;

    ConjunctProperty(final Property<T> a, final Property<T> b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public final boolean isSatisfiedBy(final T thing) {
        return a.isSatisfiedBy(thing) && b.isSatisfiedBy(thing);
    }
}
