package com.hoqhuuep.util.property;

final class NegativeProperty<T> extends Property<T> {
    private final Property<T> a;

    NegativeProperty(final Property<T> a) {
        this.a = a;
    }

    @Override
    public final boolean isSatisfiedBy(final T thing) {
        return !a.isSatisfiedBy(thing);
    }
}
