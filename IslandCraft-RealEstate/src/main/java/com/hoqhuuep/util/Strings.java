package com.hoqhuuep.util;

import org.apache.commons.lang.Validate;

public class Strings {
    /**
     * "Borrowed" from Bukkit. This method uses a region to check case-insensitive equality. This means the internal
     * array does not need to be copied like a toLowerCase() call would.
     * 
     * @param string
     *            String to check
     * @param prefix
     *            Prefix of string to compare
     * @return true if provided string starts with, ignoring case, the prefix provided
     * @throws NullPointerException
     *             if prefix is null
     * @throws IllegalArgumentException
     *             if string is null
     */
    public static boolean startsWithIgnoreCase(final String string, final String prefix) throws IllegalArgumentException, NullPointerException {
        Validate.notNull(string, "Cannot check a null string for a match");
        if (string.length() < prefix.length()) {
            return false;
        }
        return string.regionMatches(true, 0, prefix, 0, prefix.length());
    }
}
