package com.hoqhuuep.minecraft.command.parameter;

import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;

final class DoubleParameter extends Parameter<Double> {
    private double value;

    DoubleParameter(final String name) {
        super(name);
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        try {
            value = Double.parseDouble(string);
        } catch (final NumberFormatException e) {
            throw new ParameterFormatException(getFormat());
        }
    }

    @Override
    public Double getValue() {
        return value;
    }

    private String getFormat() {
        return "Argument <" + getName() + "> must be a number";
    }
}
