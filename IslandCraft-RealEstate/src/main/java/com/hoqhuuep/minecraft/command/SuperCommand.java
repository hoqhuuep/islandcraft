package com.hoqhuuep.minecraft.command;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.util.Strings;

public class SuperCommand extends Command {
    private final List<Command> subCommands;

    protected SuperCommand(final String name) {
        super(name);
        subCommands = Lists.newArrayList();
    }

    protected final void addSubCommand(final Command subCommand) {
        subCommands.add(subCommand);
    }

    @Override
    public List<String> requiredPermissions() {
        final List<String> permissions = Lists.newArrayList();
        for (final Command subCommand : subCommands) {
            permissions.addAll(subCommand.requiredPermissions());
        }
        return permissions;
    }

    @Override
    public final void onExecute(final CommandSender sender, final String[] args) throws CommandException {
        if (getAvailableSubCommands(sender).isEmpty()) {
            // Sender does not have permission
            throw new CommandPermissionException();
        }
        if (args.length < 1) {
            // No sub-command provided
            throw new CommandFormatException(getUsage(sender, args));
        }
        // Find matching sub-command
        for (final Command subCommand : subCommands) {
            if (subCommand.getName().equals(args[0])) {
                // Execute sub-command
                final String[] subArgs = Arrays.copyOfRange(args, 1, args.length);
                try {
                    subCommand.onExecute(sender, subArgs);
                } catch (final CommandFormatException e) {
                    throw new CommandFormatException(getName() + " " + e.getMessage());
                }
                // Success
                return;
            }
        }
        // Could not find a matching sub-command
        throw new CommandFormatException(getUsage(sender, args));
    }

    @Override
    public final List<String> onTabComplete(final CommandSender sender, final String[] args) {
        final String partialArg;
        if (args.length == 0) {
            partialArg = "";
        } else if (args.length == 1) {
            partialArg = args[0].toLowerCase();
        } else {
            // Find matching sub-command
            for (final Command subCommand : subCommands) {
                if (subCommand.getName().equals(args[0])) {
                    // Get completions for sub-command
                    final String[] subArgs = Arrays.copyOfRange(args, 1, args.length);
                    return subCommand.onTabComplete(sender, subArgs);
                }
            }
            // Could not find a matching sub-command
            return ImmutableList.of();
        }
        final List<String> completions = Lists.newArrayList();
        for (final String name : getAvailableSubCommands(sender)) {
            if (Strings.startsWithIgnoreCase(name, partialArg)) {
                completions.add(name);
            }
        }
        return completions;
    }

    private String getUsage(final CommandSender sender, final String[] args) {
        final StringBuilder usage = new StringBuilder();
        usage.append(getName());
        final List<String> availableSubCommands = getAvailableSubCommands(sender);
        if (availableSubCommands.size() > 0) {
            usage.append(" <");
            for (final String name : availableSubCommands) {
                usage.append(name);
                usage.append('|');
            }
            usage.deleteCharAt(usage.length() - 1);
            usage.append('>');
        }
        return usage.toString();
    }

    private List<String> getAvailableSubCommands(final CommandSender sender) {
        final List<String> availableSubCommands = Lists.newArrayList();
        for (final Command subCommand : subCommands) {
            final List<String> permissions = subCommand.requiredPermissions();
            for (final String permission : permissions) {
                if (sender.hasPermission(permission)) {
                    availableSubCommands.add(subCommand.getName());
                    break;
                }
            }
        }
        return availableSubCommands;
    }
}
