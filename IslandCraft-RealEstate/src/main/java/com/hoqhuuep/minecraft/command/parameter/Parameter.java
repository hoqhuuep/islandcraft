package com.hoqhuuep.minecraft.command.parameter;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;

public abstract class Parameter<T> {
    private final String name;

    public Parameter(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * @param player
     * @param string
     * @throws ParameterFormatException
     */
    public abstract void parse(CommandSender sender, String string) throws ParameterFormatException;

    public abstract T getValue();

    public List<String> onTabComplete(final CommandSender sender, final String string) {
        return ImmutableList.of();
    }

    public Parameter<T> orNull() {
        return new MaybeNullParameter<T>(this);
    }

    public static <T> Parameter<T> forChoice(final String name, final Map<String, T> choices) {
        return new ChoiceParameter<T>(name, choices);
    }

    private static final Map<String, Boolean> booleanChoices = ImmutableMap.of("true", true, "false", false);

    public static Parameter<Boolean> forBoolean(final String name) {
        return new ChoiceParameter<Boolean>(name, booleanChoices);
    }

    public static Parameter<Integer> forInteger(final String name) {
        return new IntegerParameter(name);
    }

    public static Parameter<Integer> forIntegerGreaterThan(final String name, final int greaterThan) {
        return new IntegerGreaterThanParameter(forInteger(name), greaterThan);
    }

    public static Parameter<Double> forDouble(final String name) {
        return new DoubleParameter(name);
    }

    public static Parameter<Double> forDoubleGreaterThan(final String name, final double greaterThan) {
        return new DoubleGreaterThanParameter(forDouble(name), greaterThan);
    }

    public static Parameter<String> forString(final String name) {
        return new StringParameter(name);
    }

    public static <S extends Enum<S>> Parameter<S> forEnum(final String name, final Class<S> clazz) {
        return new EnumParameter<S>(name, clazz);
    }

    public static Parameter<String> forPlayerName(final String name) {
        return new PlayerNameParameter(forString(name));
    }

    public static Parameter<String> forWorldName(final String name) {
        return new WorldNameParameter(forString(name));
    }
}
