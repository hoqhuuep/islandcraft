package com.hoqhuuep.minecraft.command.parameter;

import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;

final class IntegerGreaterThanParameter extends Parameter<Integer> {
    private final Parameter<Integer> delegate;
    private final int greaterThan;

    IntegerGreaterThanParameter(final Parameter<Integer> delegate, final int greaterThan) {
        super(delegate.getName());
        this.delegate = delegate;
        this.greaterThan = greaterThan;
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        delegate.parse(sender, string);
        if (!(delegate.getValue() > greaterThan)) {
            throw new ParameterFormatException(getGreaterThan());
        }
    }

    @Override
    public Integer getValue() {
        return delegate.getValue();
    }

    private String getGreaterThan() {
        return "Argument <" + getName() + "> must be greater than " + greaterThan;
    }
}
