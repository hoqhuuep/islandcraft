package com.hoqhuuep.minecraft.command;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.parameter.Parameter;

public abstract class FinalCommand extends Command {
    private final String permission;
    private final List<Parameter<?>> parameters;
    private Parameter<?> remainderParameter = null;

    protected FinalCommand(final String name, final String permission) {
        super(name);
        this.permission = permission;
        parameters = Lists.newArrayList();
    }

    protected abstract void onExecute(final CommandSender sender) throws CannotDoException;

    protected final <T extends Parameter<?>> T addParameter(final T parameter) {
        parameters.add(parameter);
        return parameter;
    }

    protected final <T extends Parameter<?>> T setRemainderParameter(final T parameter) {
        remainderParameter = parameter;
        return parameter;
    }

    @Override
    public List<String> requiredPermissions() {
        return ImmutableList.of(permission);
    }

    @Override
    public final void onExecute(final CommandSender sender, final String[] args) throws CommandException {
        if (permission != null && !sender.hasPermission(permission)) {
            // Sender does not have permission
            throw new CommandPermissionException();
        }
        if (args.length < parameters.size()) {
            // Too few arguments
            throw new CommandFormatException(getUsage());
        }
        int index = 0;
        for (final Parameter<?> parameter : parameters) {
            parameter.parse(sender, args[index++]);
        }
        if (remainderParameter != null) {
            final String remainder = StringUtils.join(Arrays.copyOfRange(args, index, args.length), " ");
            remainderParameter.parse(sender, remainder);
        } else if (args.length != parameters.size()) {
            // Too many arguments
            throw new CommandFormatException(getUsage());
        }
        // Success
        onExecute(sender);
    }

    @Override
    public final List<String> onTabComplete(final CommandSender sender, final String[] args) {
        final int numParameters = parameters.size();
        if (numParameters <= 0) {
            // No parameters => no completions
            return ImmutableList.of();
        }
        if (args.length == 0) {
            // No arguments => complete first argument
            return parameters.get(0).onTabComplete(sender, "");
        } else if (args.length <= numParameters) {
            // Up to one of the arguments => complete that arguments
            return parameters.get(args.length - 1).onTabComplete(sender, args[args.length - 1]);
        }
        // Up to remainder => complete remainder
        if (remainderParameter != null) {
            final String[] remainingArgs = Arrays.copyOfRange(args, numParameters, args.length);
            return remainderParameter.onTabComplete(sender, StringUtils.join(remainingArgs, " "));
        }
        // No remainder parameter => no completions
        return ImmutableList.of();
    }

    private String getUsage() {
        final StringBuilder usage = new StringBuilder();
        usage.append(getName());
        if (parameters.size() > 0) {
            usage.append(" <");
            for (final Parameter<?> parameter : parameters) {
                usage.append(parameter.getName());
                usage.append("> <");
            }
            usage.deleteCharAt(usage.length() - 1);
            usage.deleteCharAt(usage.length() - 1);
        }
        if (remainderParameter != null) {
            usage.append(" <");
            usage.append(remainderParameter.getName());
            usage.append("...>");
        }
        return usage.toString();
    }
}
