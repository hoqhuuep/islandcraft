package com.hoqhuuep.minecraft.command;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.chat.Chat;

public class CommandHandler {
    final Map<String, Command> commands;
    final Chat chat;

    public CommandHandler(final List<Command> commands, final Chat chat) {
        this.commands = Maps.newHashMapWithExpectedSize(commands.size());
        for (final Command command : commands) {
            this.commands.put(command.getName(), command);
        }
        this.chat = chat;
    }

    public void execute(final CommandSender sender, final String command) {
        if (sender == null) {
            // TODO localize
            chat.send(sender, "�cYou cannot do that");
            return;
        }
        try {
            final String[] args = command.split(" ", 2);
            commands.get(args[0]).onExecute(sender, args.length > 1 ? args[1].split(" ") : new String[0]);
        } catch (final CommandFormatException e) {
            // TODO localize
            chat.send(sender, "�c/" + e.getMessage());
        } catch (final CommandException e) {
            chat.send(sender, e.getMessage());
        }
    }

    public List<String> tabComplete(final CommandSender sender, final String command) {
        if (sender == null) {
            return ImmutableList.of();
        }
        final String[] args = command.split(" ", 2);
        return commands.get(args[0]).onTabComplete(sender, args.length > 1 ? args[1].split(" ") : new String[0]);
    }
}
