package com.hoqhuuep.minecraft.command.parameter;

import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;

final class IntegerParameter extends Parameter<Integer> {
    private int value;

    IntegerParameter(final String name) {
        super(name);
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        try {
            value = Integer.parseInt(string);
        } catch (final NumberFormatException e) {
            throw new ParameterFormatException(getFormat());
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    private String getFormat() {
        return "Argument <" + getName() + "> must be an integer";
    }
}
