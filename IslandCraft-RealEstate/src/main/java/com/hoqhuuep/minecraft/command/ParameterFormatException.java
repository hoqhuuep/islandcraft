package com.hoqhuuep.minecraft.command;

public class ParameterFormatException extends CommandException {
    private static final long serialVersionUID = -2891216024682137828L;

    public ParameterFormatException(final String message) {
        super(message);
    }
}
