package com.hoqhuuep.minecraft.command.parameter;

import java.util.List;

import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;
import com.hoqhuuep.util.Strings;

final class EnumParameter<T extends Enum<T>> extends Parameter<T> {
    final Class<T> clazz;
    private T value;

    EnumParameter(final String name, final Class<T> clazz) {
        super(name);
        this.clazz = clazz;
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        try {
            value = Enum.valueOf(clazz, string);
        } catch (final IllegalArgumentException e) {
            throw new ParameterFormatException(getFormat());
        }
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public List<String> onTabComplete(final CommandSender sender, final String string) {
        final List<String> completions = Lists.newArrayList();
        for (final T value : clazz.getEnumConstants()) {
            final String name = value.toString();
            if (Strings.startsWithIgnoreCase(name, string)) {
                completions.add(name);
            }
        }
        return completions;
    }

    private String getFormat() {
        final StringBuilder message = new StringBuilder();
        message.append("Argument <");
        message.append(getName());
        message.append("> must be one of: ");
        for (final T value : clazz.getEnumConstants()) {
            message.append(value.toString());
            message.append(", ");
        }
        message.deleteCharAt(message.length() - 1);
        message.deleteCharAt(message.length() - 1);
        return message.toString();
    }
}
