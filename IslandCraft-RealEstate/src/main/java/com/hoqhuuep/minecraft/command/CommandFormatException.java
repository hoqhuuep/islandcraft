package com.hoqhuuep.minecraft.command;

public class CommandFormatException extends CommandException {
    private static final long serialVersionUID = -3778935364522234374L;

    public CommandFormatException(final String message) {
        super(message);
    }
}
