package com.hoqhuuep.minecraft.command.parameter;

import java.util.List;

import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;
import com.hoqhuuep.util.Strings;

final class MaybeNullParameter<T> extends Parameter<T> {
    private final static String NULL = "~";
    private final Parameter<T> delegate;
    private boolean isNull;

    MaybeNullParameter(final Parameter<T> delegate) {
        super(delegate.getName());
        this.delegate = delegate;
        isNull = false;
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        if (string.equals(NULL)) {
            isNull = true;
            return;
        }
        delegate.parse(sender, string);
    }

    @Override
    public T getValue() {
        return isNull ? null : delegate.getValue();
    }

    @Override
    public List<String> onTabComplete(final CommandSender sender, final String string) {
        final List<String> completions = Lists.newArrayList(delegate.onTabComplete(sender, string));
        if (Strings.startsWithIgnoreCase(NULL, string)) {
            completions.add("~");
        }
        return completions;
    }
}
