package com.hoqhuuep.minecraft.command.parameter;

import java.util.List;

import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.command.ParameterFormatException;
import com.hoqhuuep.util.Strings;

final class PlayerNameParameter extends Parameter<String> {
    private final Parameter<String> delegate;

    PlayerNameParameter(final Parameter<String> delegate) {
        super(delegate.getName());
        this.delegate = delegate;
    }

    @Override
    public List<String> onTabComplete(final CommandSender sender, final String string) {
        final List<String> completions = Lists.newArrayList(delegate.onTabComplete(sender, string));
        for (final OfflinePlayer p : sender.getServer().getOfflinePlayers()) {
            final String playerName = p.getName();
            if (Strings.startsWithIgnoreCase(playerName, string)) {
                completions.add(playerName);
            }
        }
        return completions;
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        delegate.parse(sender, string);
    }

    @Override
    public String getValue() {
        return delegate.getValue();
    }
}
