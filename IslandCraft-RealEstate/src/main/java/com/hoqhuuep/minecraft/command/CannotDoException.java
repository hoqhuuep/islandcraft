package com.hoqhuuep.minecraft.command;

public class CannotDoException extends CommandException {
    private static final long serialVersionUID = 3453738379795628119L;

    public CannotDoException(final String message) {
        super(message);
    }
}
