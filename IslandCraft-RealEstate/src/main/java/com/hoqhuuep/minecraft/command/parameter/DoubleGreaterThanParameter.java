package com.hoqhuuep.minecraft.command.parameter;

import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;

final class DoubleGreaterThanParameter extends Parameter<Double> {
    private final Parameter<Double> delegate;
    private final double greaterThan;

    DoubleGreaterThanParameter(final Parameter<Double> delegate, final double greaterThan) {
        super(delegate.getName());
        this.delegate = delegate;
        this.greaterThan = greaterThan;
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        delegate.parse(sender, string);
        if (!(delegate.getValue() > greaterThan)) {
            throw new ParameterFormatException(getGreaterThan());
        }
    }

    @Override
    public Double getValue() {
        return delegate.getValue();
    }

    private String getGreaterThan() {
        return "Argument <" + getName() + "> must be greater than " + greaterThan;
    }
}
