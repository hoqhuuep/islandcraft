package com.hoqhuuep.minecraft.command;

import java.util.List;

import org.apache.commons.lang.Validate;

import com.hoqhuuep.minecraft.CommandSender;

public abstract class Command {
    private final String name;

    protected Command(final String name) {
        Validate.notEmpty(name);
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    /**
     * @return A list of permissions, a player must have at least one of these to see the command.
     */
    public abstract List<String> requiredPermissions();

    public abstract void onExecute(CommandSender sender, String[] args) throws CommandException;

    public abstract List<String> onTabComplete(CommandSender sender, String[] args);
}
