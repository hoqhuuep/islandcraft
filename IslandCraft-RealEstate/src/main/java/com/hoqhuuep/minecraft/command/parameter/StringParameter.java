package com.hoqhuuep.minecraft.command.parameter;

import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;

final class StringParameter extends Parameter<String> {
    private String value;

    StringParameter(final String name) {
        super(name);
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        if (string.isEmpty()) {
            throw new ParameterFormatException(getEmpty());
        }
        value = string;
    }

    @Override
    public String getValue() {
        return value;
    }

    private String getEmpty() {
        return "Argument <" + getName() + "> must not be empty";
    }
}
