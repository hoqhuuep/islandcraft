package com.hoqhuuep.minecraft.command;

public class CommandException extends Exception {
    private static final long serialVersionUID = 1407418526329117088L;

    public CommandException(final String message) {
        super(message);
    }
}
