package com.hoqhuuep.minecraft.command;

public class CommandPermissionException extends CommandException {
    private static final long serialVersionUID = 3642185221106135160L;

    public CommandPermissionException() {
        super("You do not have permission to use that command");
    }
}
