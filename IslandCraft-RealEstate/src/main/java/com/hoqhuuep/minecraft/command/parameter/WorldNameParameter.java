package com.hoqhuuep.minecraft.command.parameter;

import java.util.List;

import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.World;
import com.hoqhuuep.minecraft.command.ParameterFormatException;
import com.hoqhuuep.util.Strings;

final class WorldNameParameter extends Parameter<String> {
    private final Parameter<String> delegate;

    WorldNameParameter(final Parameter<String> delegate) {
        super(delegate.getName());
        this.delegate = delegate;
    }

    @Override
    public List<String> onTabComplete(final CommandSender sender, final String string) {
        final List<String> completions = Lists.newArrayList(delegate.onTabComplete(sender, string));
        for (final World world : sender.getServer().getWorlds()) {
            final String worldName = world.getName();
            if (Strings.startsWithIgnoreCase(worldName, string)) {
                completions.add(worldName);
            }
        }
        return completions;
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        delegate.parse(sender, string);
    }

    @Override
    public String getValue() {
        return delegate.getValue();
    }
}
