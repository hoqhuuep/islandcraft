package com.hoqhuuep.minecraft.command.parameter;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.command.ParameterFormatException;
import com.hoqhuuep.util.Strings;

final class ChoiceParameter<T> extends Parameter<T> {
    private final Map<String, T> choices;
    private T value;

    ChoiceParameter(final String name, final Map<String, T> choices) {
        super(name);
        this.choices = ImmutableMap.copyOf(choices);
    }

    @Override
    public void parse(final CommandSender sender, final String string) throws ParameterFormatException {
        for (final String choice : choices.keySet()) {
            if (string.equals(choice)) {
                value = choices.get(choice);
                return;
            }
        }
        throw new ParameterFormatException(getFormat());
    }

    @Override
    public List<String> onTabComplete(final CommandSender sender, final String string) {
        final List<String> completions = Lists.newArrayList();
        for (final String choice : choices.keySet()) {
            if (Strings.startsWithIgnoreCase(choice, string)) {
                completions.add(choice);
            }
        }
        return completions;
    }

    @Override
    public T getValue() {
        return value;
    }

    private String getFormat() {
        final StringBuilder message = new StringBuilder();
        message.append("Argument <");
        message.append(getName());
        message.append("> must be one of: ");
        message.append(StringUtils.join(choices.keySet(), ", "));
        return message.toString();
    }
}
