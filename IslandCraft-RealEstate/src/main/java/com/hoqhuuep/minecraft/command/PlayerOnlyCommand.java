package com.hoqhuuep.minecraft.command;

import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.Player;

public abstract class PlayerOnlyCommand extends FinalCommand {
    protected PlayerOnlyCommand(final String name, final String permission) {
        super(name, permission);
    }

    protected abstract void onExecute(final Player player) throws CannotDoException;

    @Override
    protected void onExecute(final CommandSender sender) throws CannotDoException {
        if (!(sender instanceof Player)) {
            // TODO localize
            throw new CannotDoException("�cYou must be a player to use that command");
        }
        final Player player = (Player) sender;
        onExecute(player);
    }
}
