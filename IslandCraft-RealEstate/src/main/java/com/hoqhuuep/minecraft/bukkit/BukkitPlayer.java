package com.hoqhuuep.minecraft.bukkit;

import java.util.UUID;

import org.apache.commons.lang.Validate;

import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.Location5;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.World;

public final class BukkitPlayer extends BukkitCommandSender implements Player {
    final org.bukkit.entity.Player bukkitPlayer;

    public BukkitPlayer(final org.bukkit.entity.Player bukkitPlayer) {
        super(bukkitPlayer);
        Validate.notNull(bukkitPlayer);
        this.bukkitPlayer = bukkitPlayer;
    }

    @Override
    public UUID getUUID() {
        return bukkitPlayer.getUniqueId();
    }

    @Override
    public String getName() {
        return bukkitPlayer.getName();
    }

    @Override
    public Location getLocation() {
        final org.bukkit.Location bukkitLocation = bukkitPlayer.getLocation();
        final World world = BukkitFactory.fromBukkitWorld(bukkitLocation.getWorld());
        final int x = bukkitLocation.getBlockX();
        final int z = bukkitLocation.getBlockZ();
        return new Location(world, x, z);
    }

    @Override
    public Location5 getLocation5() {
        final org.bukkit.Location bukkitLocation = bukkitPlayer.getLocation();
        final World world = BukkitFactory.fromBukkitWorld(bukkitLocation.getWorld());
        final double x = bukkitLocation.getX();
        final double y = bukkitLocation.getY();
        final double z = bukkitLocation.getZ();
        final float yaw = bukkitLocation.getYaw();
        final float pitch = bukkitLocation.getPitch();
        return new Location5(world, x, y, z, yaw, pitch);
    }

    @Override
    public World getWorld() {
        return BukkitFactory.fromBukkitWorld(bukkitPlayer.getWorld());
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof OfflinePlayer) {
            OfflinePlayer player = (OfflinePlayer) object;
            return getUUID().equals(player.getUUID());
        }
        return false;
    }

    @Override
    public Player getPlayer() {
        return this;
    }

    @Override
    public void teleport(final Location5 location) {
        final World world = location.getWorld();
        final double x = location.getX();
        final double y = location.getY();
        final double z = location.getZ();
        final float yaw = location.getYaw();
        final float pitch = location.getPitch();
        final org.bukkit.World bukkitWorld = BukkitFactory.toBukkitWorld(world);
        final org.bukkit.Location bukkitLocation = new org.bukkit.Location(bukkitWorld, x, y, z, yaw, pitch);
        bukkitPlayer.teleport(bukkitLocation);
    }
}
