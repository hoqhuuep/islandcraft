package com.hoqhuuep.minecraft.bukkit;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.Validate;

import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.World;

public final class BukkitServer extends BukkitCommandSender implements Server {
    private final org.bukkit.Server bukkitServer;

    public BukkitServer(final org.bukkit.Server bukkitServer) {
        super(bukkitServer.getConsoleSender());
        Validate.notNull(bukkitServer);
        this.bukkitServer = bukkitServer;
    }

    @Override
    public Player getPlayer(final UUID uuid) {
        Validate.notNull(uuid);
        return BukkitFactory.fromBukkitPlayer(bukkitServer.getPlayer(uuid));
    }

    @Override
    public Player getPlayer(final String name) {
        Validate.notEmpty(name);
        return BukkitFactory.fromBukkitPlayer(bukkitServer.getPlayer(name));
    }

    @Override
    public OfflinePlayer getOfflinePlayer(final UUID uuid) {
        Validate.notNull(uuid);
        for (final org.bukkit.OfflinePlayer bukkitOfflinePlayer : bukkitServer.getOfflinePlayers()) {
            if (uuid.equals(bukkitOfflinePlayer.getUniqueId())) {
                return BukkitFactory.fromBukkitOfflinePlayer(bukkitOfflinePlayer);
            }
        }
        return null;
    }

    @Override
    public OfflinePlayer getOfflinePlayer(final String name) {
        Validate.notEmpty(name);
        for (final org.bukkit.OfflinePlayer bukkitOfflinePlayer : bukkitServer.getOfflinePlayers()) {
            // NOTE names are not case sensitive for uniqueness
            if (name.equalsIgnoreCase(bukkitOfflinePlayer.getName())) {
                return BukkitFactory.fromBukkitOfflinePlayer(bukkitOfflinePlayer);
            }
        }
        return null;
    }

    @Override
    public World getWorld(final UUID uid) {
        Validate.notNull(uid);
        return BukkitFactory.fromBukkitWorld(bukkitServer.getWorld(uid));
    }

    @Override
    public World getWorld(final String name) {
        Validate.notEmpty(name);
        return BukkitFactory.fromBukkitWorld(bukkitServer.getWorld(name));
    }

    @Override
    public List<World> getWorlds() {
        return BukkitFactory.fromBukkitWorlds(bukkitServer.getWorlds());
    }

    @Override
    public List<Player> getPlayers() {
        return BukkitFactory.fromBukkitPlayers(Arrays.asList(bukkitServer.getOnlinePlayers()));
    }

    @Override
    public List<OfflinePlayer> getOfflinePlayers() {
        final org.bukkit.OfflinePlayer[] bukkitOfflinePlayers = bukkitServer.getOfflinePlayers();
        final List<OfflinePlayer> offlinePlayers = Lists.newArrayListWithCapacity(bukkitOfflinePlayers.length);
        for (final org.bukkit.OfflinePlayer bukkitOfflinePlayer : bukkitOfflinePlayers) {
            offlinePlayers.add(BukkitFactory.fromBukkitOfflinePlayer(bukkitOfflinePlayer));
        }
        return offlinePlayers;
    }
}
