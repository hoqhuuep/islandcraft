package com.hoqhuuep.minecraft.bukkit;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.World;

public final class BukkitFactory {
    public static final Map<org.bukkit.Server, Server> servers = Maps.newHashMap();
    public static final Map<org.bukkit.World, World> worlds = Maps.newHashMap();
    public static final Map<org.bukkit.entity.Player, Player> players = Maps.newHashMap();
    public static final Map<org.bukkit.command.CommandSender, CommandSender> senders = Maps.newHashMap();
    public static final Map<org.bukkit.OfflinePlayer, OfflinePlayer> offlinePlayers = Maps.newHashMap();

    public static Player fromBukkitPlayer(final org.bukkit.entity.Player bukkitPlayer) {
        Validate.notNull(bukkitPlayer);
        final Player cachedPlayer = players.get(bukkitPlayer);
        if (cachedPlayer == null) {
            final Player newPlayer = new BukkitPlayer(bukkitPlayer);
            players.put(bukkitPlayer, newPlayer);
            return newPlayer;
        }
        return cachedPlayer;
    }

    public static OfflinePlayer fromBukkitOfflinePlayer(final org.bukkit.OfflinePlayer bukkitOfflinePlayer) {
        Validate.notNull(bukkitOfflinePlayer);
        final OfflinePlayer cachedOfflinePlayer = offlinePlayers.get(bukkitOfflinePlayer);
        if (cachedOfflinePlayer == null) {
            final OfflinePlayer newOfflinePlayer = new BukkitOfflinePlayer(bukkitOfflinePlayer);
            offlinePlayers.put(bukkitOfflinePlayer, newOfflinePlayer);
            return newOfflinePlayer;
        }
        return cachedOfflinePlayer;
    }

    public static World fromBukkitWorld(final org.bukkit.World bukkitWorld) {
        Validate.notNull(bukkitWorld);
        final World cachedWorld = worlds.get(bukkitWorld);
        if (cachedWorld == null) {
            final World newWorld = new BukkitWorld(bukkitWorld);
            worlds.put(bukkitWorld, newWorld);
            return newWorld;
        }
        return cachedWorld;
    }

    public static Server fromBukkitServer(final org.bukkit.Server bukkitServer) {
        Validate.notNull(bukkitServer);
        final Server cachedServer = servers.get(bukkitServer);
        if (cachedServer == null) {
            final Server newServer = new BukkitServer(bukkitServer);
            servers.put(bukkitServer, newServer);
            return newServer;
        }
        return cachedServer;
    }

    public static List<Player> fromBukkitPlayers(final List<org.bukkit.entity.Player> bukkitPlayers) {
        Validate.notNull(bukkitPlayers);
        final List<Player> players = Lists.newArrayListWithCapacity(bukkitPlayers.size());
        for (final org.bukkit.entity.Player bukkitPlayer : bukkitPlayers) {
            final Player player = fromBukkitPlayer(bukkitPlayer);
            players.add(player);
        }
        return players;
    }

    public static List<World> fromBukkitWorlds(final List<org.bukkit.World> bukkitWorlds) {
        Validate.notNull(bukkitWorlds);
        final List<World> worlds = Lists.newArrayListWithCapacity(bukkitWorlds.size());
        for (final org.bukkit.World bukkitWorld : bukkitWorlds) {
            final World world = fromBukkitWorld(bukkitWorld);
            worlds.add(world);
        }
        return worlds;
    }

    public static List<Server> fromBukkitServers(final List<org.bukkit.Server> bukkitServers) {
        Validate.notNull(bukkitServers);
        final List<Server> servers = Lists.newArrayListWithCapacity(bukkitServers.size());
        for (final org.bukkit.Server bukkitServer : bukkitServers) {
            final Server server = fromBukkitServer(bukkitServer);
            servers.add(server);
        }
        return servers;
    }

    public static org.bukkit.World toBukkitWorld(final World world) {
        return Bukkit.getWorld(world.getName());
    }

    public static CommandSender fromBukkitSender(final org.bukkit.command.CommandSender bukkitSender) {
        Validate.notNull(bukkitSender);
        if (bukkitSender instanceof org.bukkit.entity.Player) {
            return fromBukkitPlayer((org.bukkit.entity.Player) bukkitSender);
        }
        if (bukkitSender instanceof org.bukkit.command.ConsoleCommandSender) {
            return fromBukkitServer(bukkitSender.getServer());
        }
        final CommandSender cachedSender = senders.get(bukkitSender);
        if (cachedSender == null) {
            final CommandSender newSender = new BukkitCommandSender(bukkitSender);
            senders.put(bukkitSender, newSender);
            return newSender;
        }
        return cachedSender;
    }
}
