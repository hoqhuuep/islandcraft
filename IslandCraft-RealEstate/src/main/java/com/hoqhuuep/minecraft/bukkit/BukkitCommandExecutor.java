package com.hoqhuuep.minecraft.bukkit;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.command.TabExecutor;

import com.hoqhuuep.minecraft.command.CommandHandler;

public class BukkitCommandExecutor implements TabExecutor {
    private final CommandHandler executor;

    public BukkitCommandExecutor(final CommandHandler executor) {
        this.executor = executor;
    }

    @Override
    public final boolean onCommand(final org.bukkit.command.CommandSender bukkitSender, final org.bukkit.command.Command command, final String alias, final String[] args) {
        executor.execute(BukkitFactory.fromBukkitSender(bukkitSender), command.getName() + " " + StringUtils.join(args, " "));
        return true;
    }

    @Override
    public final List<String> onTabComplete(final org.bukkit.command.CommandSender bukkitSender, final org.bukkit.command.Command command, final String alias, final String[] args) {
        return executor.tabComplete(BukkitFactory.fromBukkitSender(bukkitSender), command.getName() + " " + StringUtils.join(args, " "));
    }
}
