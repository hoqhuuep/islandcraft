package com.hoqhuuep.minecraft.bukkit;

import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.Server;

public class BukkitCommandSender implements CommandSender {
    final org.bukkit.command.CommandSender bukkitCommandSender;

    public BukkitCommandSender(final org.bukkit.command.CommandSender bukkitCommandSender) {
        this.bukkitCommandSender = bukkitCommandSender;
    }

    @Override
    public final boolean hasPermission(final String permission) {
        return bukkitCommandSender.hasPermission(permission);
    }

    @Override
    public final Server getServer() {
        return BukkitFactory.fromBukkitServer(bukkitCommandSender.getServer());
    }

    @Override
    public final void sendMessage(final String message) {
        bukkitCommandSender.sendMessage(message);
    }
}
