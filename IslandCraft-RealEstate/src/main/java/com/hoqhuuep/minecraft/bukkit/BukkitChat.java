package com.hoqhuuep.minecraft.bukkit;

import com.hoqhuuep.minecraft.CommandSender;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.chat.Chat;

public class BukkitChat implements Chat {
    private final org.bukkit.Server bukkitServer;

    public BukkitChat(final org.bukkit.Server bukkitServer) {
        this.bukkitServer = bukkitServer;
    }

    @Override
    public void send(final CommandSender to, final String message) {
        if (to instanceof Player) {
            bukkitServer.getPlayer(((Player) to).getUUID()).sendMessage(message);
        } else if (to instanceof Server) {
            bukkitServer.getConsoleSender().sendMessage(message);
        }
    }
}
