package com.hoqhuuep.minecraft.bukkit;

import java.util.UUID;

import org.apache.commons.lang.Validate;

import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.Player;

public final class BukkitOfflinePlayer implements OfflinePlayer {
    final org.bukkit.OfflinePlayer bukkitOfflinePlayer;

    public BukkitOfflinePlayer(final org.bukkit.OfflinePlayer bukkitOfflinePlayer) {
        Validate.notNull(bukkitOfflinePlayer);
        this.bukkitOfflinePlayer = bukkitOfflinePlayer;
    }

    @Override
    public UUID getUUID() {
        return bukkitOfflinePlayer.getUniqueId();
    }

    @Override
    public String getName() {
        return bukkitOfflinePlayer.getName();
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof OfflinePlayer) {
            OfflinePlayer player = (OfflinePlayer) object;
            return getUUID().equals(player.getUUID());
        }
        return false;
    }

    @Override
    public Player getPlayer() {
        final org.bukkit.entity.Player player = bukkitOfflinePlayer.getPlayer();
        if (player == null) {
            return null;
        }
        return BukkitFactory.fromBukkitPlayer(player);
    }
}
