package com.hoqhuuep.minecraft.bukkit;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;

import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.World;

public final class BukkitWorld implements World {
    private final org.bukkit.World bukkitWorld;

    public BukkitWorld(final org.bukkit.World bukkitWorld) {
        Validate.notNull(bukkitWorld);
        this.bukkitWorld = bukkitWorld;
    }

    @Override
    public UUID getUID() {
        return bukkitWorld.getUID();
    }

    @Override
    public String getName() {
        return bukkitWorld.getName();
    }

    @Override
    public long getSeed() {
        return bukkitWorld.getSeed();
    }

    @Override
    public Server getServer() {
        // TODO why doesn't org.bukkit.World have a getServer method?
        return BukkitFactory.fromBukkitServer(Bukkit.getServer());
    }

    @Override
    public List<Player> getPlayers() {
        return BukkitFactory.fromBukkitPlayers(bukkitWorld.getPlayers());
    }

    @Override
    public void regenerateChunk(final int x, final int z) {
        bukkitWorld.regenerateChunk(x, z);
    }

    @Override
    public int getSurfaceY(final int x, final int z) {
        return bukkitWorld.getHighestBlockYAt(x, z);
    }
}
