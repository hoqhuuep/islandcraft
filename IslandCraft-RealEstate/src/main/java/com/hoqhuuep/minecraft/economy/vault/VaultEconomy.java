package com.hoqhuuep.minecraft.economy.vault;

import org.apache.commons.lang.Validate;

import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.World;
import com.hoqhuuep.minecraft.economy.Economy;

public class VaultEconomy implements Economy {
    private final net.milkbowl.vault.economy.Economy vaultEconomy;

    public VaultEconomy(final net.milkbowl.vault.economy.Economy vaultEconomy) {
        Validate.notNull(vaultEconomy);
        this.vaultEconomy = vaultEconomy;
    }

    private final boolean withdraw(final OfflinePlayer player, final World world, final double amount) {
        // Vault uses player names rather than UUIDs
        final String playerName = player.getName();
        // Vault uses world names rather than UIDs
        final String worldName = world.getName();
        return vaultEconomy.withdrawPlayer(playerName, worldName, amount).transactionSuccess();
    }

    private final boolean deposit(final OfflinePlayer player, final World world, final double amount) {
        // Vault uses player names rather than UUIDs
        final String playerName = player.getName();
        // Vault uses world names rather than UIDs
        final String worldName = world.getName();
        return vaultEconomy.depositPlayer(playerName, worldName, amount).transactionSuccess();
    }

    @Override
    public boolean transfer(final OfflinePlayer from, final OfflinePlayer to, final World world, double amount) {
        Validate.notNull(world);
        if (from != null && !withdraw(from, world, amount)) {
            return false;
        }
        if (to != null && !deposit(to, world, amount)) {
            // Undo withdraw, should never fail
            if (from != null) {
                deposit(from, world, amount);
            }
            return false;
        }
        // Success
        return true;
    }

    @Override
    public final String format(final double amount) {
        return vaultEconomy.format(amount);
    }
}
