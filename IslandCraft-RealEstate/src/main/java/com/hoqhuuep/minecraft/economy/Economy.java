package com.hoqhuuep.minecraft.economy;

import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.World;

/**
 * Allows interfacing with a Minecraft economy system. Allows for moving money in to, out of and between accounts.
 * Accounts are specified by a player and a world. Money quantities are represented double precision floating point
 * values.
 */
public interface Economy {
    /**
     * Attempts to transfer an amount from one player's account in a world to another player's account in a world.
     * 
     * @param from
     *            Player from whose account the money should be withdrawn. If null, money is created.
     * @param to
     *            Player to whose account the money should be deposited. If null, money is destroyed.
     * @param world
     *            World to which the accounts are associated. Must not be null.
     * @param amount
     *            Quantity of money to transfer.
     * @return True if the money was successfully transfered. False otherwise; neither account balance will be changed.
     * @throws IllegalArgumentException
     *             Thrown if the given world is null.
     */
    boolean transfer(OfflinePlayer from, OfflinePlayer to, World world, double amount);

    /**
     * Formats a money quantity into a suitable format for display purposes.
     * 
     * @param amount
     *            Quantity of money.
     * @return An appropriately formatted string. For example: "$10" or "7 emeralds".
     */
    String format(double amount);
}
