package com.hoqhuuep.minecraft;

import java.util.List;
import java.util.UUID;

public interface World {
    UUID getUID();

    String getName();

    long getSeed();

    Server getServer();

    List<Player> getPlayers();

    void regenerateChunk(int x, int z);

    int getSurfaceY(int x, int z);
}
