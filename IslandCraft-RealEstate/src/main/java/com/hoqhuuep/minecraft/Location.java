package com.hoqhuuep.minecraft;

public final class Location {
    private final World world;
    private final int x;
    private final int z;

    public Location(final World world, final int x, final int z) {
        this.world = world;
        this.x = x;
        this.z = z;
    }

    public World getWorld() {
        return world;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    @Override
    public String toString() {
        return "Location(x: " + x + ", z: " + z + ")";
    }

    @Override
    public boolean equals(final Object object) {
        if (object == null || !(object instanceof Location) || world == null) {
            return false;
        }
        final Location that = (Location) object;
        return x == that.x && z == that.z && world.equals(that.world);
    }
}
