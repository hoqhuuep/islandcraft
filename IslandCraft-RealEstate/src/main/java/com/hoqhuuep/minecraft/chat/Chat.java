package com.hoqhuuep.minecraft.chat;

import com.hoqhuuep.minecraft.CommandSender;

public interface Chat {
    void send(final CommandSender to, final String message);
}
