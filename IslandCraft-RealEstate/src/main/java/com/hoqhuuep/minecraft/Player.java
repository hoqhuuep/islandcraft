package com.hoqhuuep.minecraft;

public interface Player extends OfflinePlayer, CommandSender {
    Location getLocation();

    Location5 getLocation5();

    World getWorld();

    void teleport(Location5 warp);
}
