package com.hoqhuuep.minecraft;

public interface CommandSender {
    boolean hasPermission(String permission);

    Server getServer();

    void sendMessage(String message);
}
