package com.hoqhuuep.minecraft;

import java.util.UUID;

public interface OfflinePlayer {
    UUID getUUID();

    String getName();

    Player getPlayer();
}
