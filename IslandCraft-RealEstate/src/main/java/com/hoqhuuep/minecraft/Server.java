package com.hoqhuuep.minecraft;

import java.util.List;
import java.util.UUID;

public interface Server extends CommandSender {
    Player getPlayer(final UUID uuid);

    Player getPlayer(final String name);

    OfflinePlayer getOfflinePlayer(final UUID uuid);

    OfflinePlayer getOfflinePlayer(final String name);

    World getWorld(final UUID uid);

    World getWorld(final String name);

    List<World> getWorlds();

    List<Player> getPlayers();

    List<OfflinePlayer> getOfflinePlayers();
}
