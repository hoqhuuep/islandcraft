package com.hoqhuuep.islandcraft;

import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.World;
import com.hoqhuuep.util.property.Property;

public abstract class IslandProperty extends Property<Island> {
    public static final Property<Island> forWorld(final World world) {
        return new WorldIslandProperty(world);
    }

    public static final Property<Island> forOwner(final OfflinePlayer owner) {
        return new OwnerIslandProperty(owner);
    }

    public static final Property<Island> forLocation(final Location location) {
        return new LocationIslandProperty(location);
    }
}
