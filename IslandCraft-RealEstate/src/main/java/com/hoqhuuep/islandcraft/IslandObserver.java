package com.hoqhuuep.islandcraft;

public interface IslandObserver {
    void onUpdate(Island island, EstateAgent estateAgent);
}
