package com.hoqhuuep.islandcraft;

public interface Phrases {
    String errorNotOwner();

    String errorAlreadyOwner();

    String errorNotForSale();

    String errorNotOnIsland();

    String errorNotInIslandCraftWorld();

    String errorMaxIslands();

    String errorTransferFailed();

    String errorWithdrawFailed();

    String errorNoIslands();

    String errorUnknownPlayer();

    String errorNoSuchIsland();

    String success();

    String islandEnter(String description);

    String islandLeave(String description);

    String islandInfo(String description);

    String islandList(int i, String description);

    String untitledIsland();

    String reservedIsland();

    String description(String name);

    String descriptionWithOwner(String name, String owner);

    String descriptionWithPrice(String name, String price);

    String descriptionWithOwnerAndPrice(String name, String owner, String price);
}
