package com.hoqhuuep.islandcraft;

import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.Location5;
import com.hoqhuuep.minecraft.OfflinePlayer;

public class Island {
    private Location location;
    private String name;
    private OfflinePlayer owner;
    private Double price;
    private Location5 warp;

    public Island(final Island island) {
        location = island.location;
        name = island.name;
        owner = island.owner;
        price = island.price;
        warp = island.warp;
    }

    public Island(final Location location, final String name, final OfflinePlayer owner, final Double price, final Location5 warp) {
        this.location = location;
        this.name = name;
        this.owner = owner;
        this.price = price;
        this.warp = warp;
    }

    public Location getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public OfflinePlayer getOwner() {
        return owner;
    }

    public Double getPrice() {
        return price;
    }

    public Location5 getWarp() {
        return warp;
    }

    public void setLocation(final Location location) {
        this.location = location;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setOwner(final OfflinePlayer owner) {
        this.owner = owner;
    }

    public void setPrice(final Double price) {
        this.price = price;
    }

    public void setWarp(final Location5 warp) {
        this.warp = warp;
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof Island) {
            final Island island = (Island) object;
            return location.equals(island.getLocation());
        }
        return false;
    }

    @Override
    public String toString() {
        return "Island(location: " + location + ", name: " + name + ", owner: " + owner + ", price: " + price + ", warp: " + warp + ")";
    }
}
