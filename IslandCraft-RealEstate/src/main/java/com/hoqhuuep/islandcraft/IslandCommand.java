package com.hoqhuuep.islandcraft;

import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.chat.Chat;
import com.hoqhuuep.minecraft.command.CannotDoException;
import com.hoqhuuep.minecraft.command.PlayerOnlyCommand;
import com.hoqhuuep.minecraft.command.SuperCommand;
import com.hoqhuuep.minecraft.command.parameter.Parameter;

public class IslandCommand extends SuperCommand {
    public IslandCommand(final EstateAgent estateAgent, final Chat chat, final Phrases phrases) {
        super("island");
        addSubCommand(new PlayerOnlyCommand("buy", "islandcraft.command.island.buy") {
            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                final Island island = estateAgent.islandAt(player.getLocation());
                estateAgent.buyIsland(player, island);
                chat.send(player, phrases.success());
            }
        });
        addSubCommand(new PlayerOnlyCommand("abandon", "islandcraft.command.island.abandon") {
            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                final Island island = estateAgent.islandAt(player.getLocation());
                estateAgent.abandonIsland(player, island);
                chat.send(player, phrases.success());
            }
        });
        addSubCommand(new PlayerOnlyCommand("sell", "islandcraft.command.island.sell") {
            private final Parameter<Double> price = addParameter(Parameter.forDoubleGreaterThan("amount", 0.0));

            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                final Island island = estateAgent.islandAt(player.getLocation());
                estateAgent.sellIsland(player, island, price.getValue());
                chat.send(player, phrases.success());
            }
        });
        addSubCommand(new PlayerOnlyCommand("unsell", "islandcraft.command.island.unsell") {
            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                final Island island = estateAgent.islandAt(player.getLocation());
                estateAgent.sellIsland(player, island, null);
                chat.send(player, phrases.success());
            }
        });
        addSubCommand(new PlayerOnlyCommand("rename", "islandcraft.command.island.rename") {
            private final Parameter<String> name = setRemainderParameter(Parameter.forString("name").orNull());

            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                final Island island = estateAgent.islandAt(player.getLocation());
                estateAgent.renameIsland(player, island, name.getValue());
                chat.send(player, phrases.success());
            }
        });
        addSubCommand(new PlayerOnlyCommand("info", "islandcraft.command.island.info") {
            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                final Island island = estateAgent.islandAt(player.getLocation());
                estateAgent.examineIsland(player, island);
            }
        });
        addSubCommand(new PlayerOnlyCommand("list", "islandcraft.command.island.list") {
            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                estateAgent.listIslands(player);
            }
        });
        addSubCommand(new PlayerOnlyCommand("setwarp", "islandcraft.command.island.setwarp") {
            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                final Island island = estateAgent.islandAt(player.getLocation());
                estateAgent.setIslandWarp(player, island, player.getLocation5());
                chat.send(player, phrases.success());
            }
        });
        addSubCommand(new PlayerOnlyCommand("warp", "islandcraft.command.island.warp") {
            private final Parameter<String> owner = addParameter(Parameter.forPlayerName("owner"));
            private final Parameter<Integer> index = addParameter(Parameter.forIntegerGreaterThan("index", 0));

            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                estateAgent.warpToIsland(player, owner.getValue(), index.getValue());
                chat.send(player, phrases.success());
            }
        });
        addSubCommand(new PlayerOnlyCommand("regenerate", "islandcraft.command.island.regenerate") {
            @Override
            protected final void onExecute(final Player player) throws CannotDoException {
                final Island island = estateAgent.islandAt(player.getLocation());
                estateAgent.regenerateIsland(island);
                chat.send(player, phrases.success());
            }
        });
        addSubCommand(new SuperCommand("set") {
            {
                addSubCommand(new PlayerOnlyCommand("name", "islandcraft.command.island.set.name") {
                    private final Parameter<String> name = setRemainderParameter(Parameter.forString("name").orNull());

                    @Override
                    protected final void onExecute(final Player player) throws CannotDoException {
                        final Island island = estateAgent.islandAt(player.getLocation());
                        estateAgent.overrideIslandName(island, name.getValue());
                        chat.send(player, phrases.success());
                    }
                });
                addSubCommand(new PlayerOnlyCommand("owner", "islandcraft.command.island.set.owner") {
                    private final Parameter<String> owner = addParameter(Parameter.forPlayerName("owner").orNull());

                    @Override
                    protected final void onExecute(final Player player) throws CannotDoException {
                        final Island island = estateAgent.islandAt(player.getLocation());
                        estateAgent.overrideIslandOwner(island, owner.getValue());
                        chat.send(player, phrases.success());
                    }
                });
                addSubCommand(new PlayerOnlyCommand("price", "islandcraft.command.island.set.price") {
                    private final Parameter<Double> price = addParameter(Parameter.forDoubleGreaterThan("price", 0.0).orNull());

                    @Override
                    protected final void onExecute(final Player player) throws CannotDoException {
                        final Island island = estateAgent.islandAt(player.getLocation());
                        estateAgent.overrideIslandPrice(island, price.getValue());
                        chat.send(player, phrases.success());
                    }
                });
                addSubCommand(new PlayerOnlyCommand("warp", "islandcraft.command.island.set.warp") {
                    @Override
                    protected final void onExecute(final Player player) throws CannotDoException {
                        final Island island = estateAgent.islandAt(player.getLocation());
                        estateAgent.overrideIslandWarp(island, player.getLocation5());
                        chat.send(player, phrases.success());
                    }
                });
            }
        });
    }
}
