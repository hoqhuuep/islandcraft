package com.hoqhuuep.islandcraft;

import org.apache.commons.lang.ObjectUtils;

import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.util.property.Property;

public class LocationIslandProperty extends Property<Island> {
    private final Location location;

    public LocationIslandProperty(final Location location) {
        this.location = location;
    }

    public final Location getLocation() {
        return location;
    }

    @Override
    public final boolean isSatisfiedBy(final Island island) {
        return ObjectUtils.equals(island.getLocation(), location);
    }
}
