package com.hoqhuuep.islandcraft.ebean;

import java.util.List;
import java.util.UUID;

import com.avaje.ebean.EbeanServer;
import com.google.common.collect.ImmutableList;
import com.hoqhuuep.islandcraft.Island;
import com.hoqhuuep.islandcraft.LocationIslandProperty;
import com.hoqhuuep.islandcraft.OwnerIslandProperty;
import com.hoqhuuep.islandcraft.WorldIslandProperty;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.Location5;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.World;
import com.hoqhuuep.util.property.Property;
import com.hoqhuuep.util.repository.ebean.EbeanRepository;

public class IslandEbeanRepository extends EbeanRepository<Island, IslandBean> {
    final Server server;

    public IslandEbeanRepository(final EbeanServer ebean, final Server server) {
        super(ebean, IslandBean.class);
        this.server = server;
    }

    @Override
    public final void save(final Island island) {
        final IslandBean bean = toBean(island);
        final IslandBean toUpdate = ebean.find(IslandBean.class, bean.getLocation());
        if (toUpdate == null) {
            // New entry, use insert
            System.out.println("INSERT ISLAND (" + bean.getLocation().getX() + ", " + bean.getLocation().getZ() + ")");
            System.out.println(bean.getName());
            System.out.println(bean.getOwnerUUID());
            System.out.println(bean.getPrice());
            ebean.insert(bean);
        } else {
            // Existing entry, use update
            System.out.println("UPDATE ISLAND (" + bean.getLocation().getX() + ", " + bean.getLocation().getZ() + ")");
            System.out.println(bean.getName());
            System.out.println(bean.getOwnerUUID());
            System.out.println(bean.getPrice());
            toUpdate.setName(bean.getName());
            toUpdate.setOwnerUUID(bean.getOwnerUUID());
            toUpdate.setPrice(bean.getPrice());
            toUpdate.setWarpWorld(bean.getWarpWorld());
            toUpdate.setWarpX(bean.getWarpX());
            toUpdate.setWarpY(bean.getWarpY());
            toUpdate.setWarpZ(bean.getWarpZ());
            toUpdate.setWarpYaw(bean.getWarpYaw());
            toUpdate.setWarpPitch(bean.getWarpPitch());
            ebean.save(toUpdate);
        }
    }

    @Override
    public List<Island> load(final Property<Island> property) {
        if (property instanceof LocationIslandProperty) {
            final LocationIslandProperty locationIslandProperty = (LocationIslandProperty) property;
            final Location location = locationIslandProperty.getLocation();
            final IslandBean bean = ebean.find(IslandBean.class, toLocationBean(location));
            if (bean == null) {
                return ImmutableList.of();
            }
            return beansToThings(ImmutableList.of(bean));
        } else if (property instanceof WorldIslandProperty) {
            final WorldIslandProperty worldIslandProperty = (WorldIslandProperty) property;
            final String worldName = worldIslandProperty.getWorld().getName();
            final List<IslandBean> beans = ebean.find(IslandBean.class).where().eq("world", worldName).findList();
            return beansToThings(beans);
        } else if (property instanceof OwnerIslandProperty) {
            final OwnerIslandProperty ownerIslandProperty = (OwnerIslandProperty) property;
            final UUID ownerUUID = ownerIslandProperty.getOwner().getUUID();
            final List<IslandBean> beans = ebean.find(IslandBean.class).where().eq("ownerUUID", ownerUUID).findList();
            return beansToThings(beans);
        } else {
            return super.load(property);
        }
    }

    private final Location fromLocationBean(final LocationBean bean) {
        return new Location(server.getWorld(bean.getWorld()), bean.getX(), bean.getZ());
    }

    private final LocationBean toLocationBean(final Location location) {
        final LocationBean bean = new LocationBean();
        bean.setWorld(location.getWorld().getName());
        bean.setX(location.getX());
        bean.setZ(location.getZ());
        return bean;
    }

    @Override
    public final Island fromBean(final IslandBean bean) {
        final Location5 warp;
        final String warpWorld = bean.getWarpWorld();
        if (warpWorld == null) {
            warp = null;
        } else {
            warp = new Location5(server.getWorld(bean.getWarpWorld()), bean.getWarpX(), bean.getWarpY(), bean.getWarpZ(), bean.getWarpYaw(), bean.getWarpPitch());
        }
        final UUID ownerUUID = bean.getOwnerUUID();
        final OfflinePlayer owner = ownerUUID == null ? null : server.getOfflinePlayer(ownerUUID);
        return new Island(fromLocationBean(bean.getLocation()), bean.getName(), owner, bean.getPrice(), warp);
    }

    @Override
    public final IslandBean toBean(final Island island) {
        final IslandBean bean = new IslandBean();
        bean.setLocation(toLocationBean(island.getLocation()));
        bean.setName(island.getName());
        final OfflinePlayer owner = island.getOwner();
        if (owner == null) {
            bean.setOwnerUUID(null);
        } else {
            bean.setOwnerUUID(owner.getUUID());
        }
        bean.setPrice(island.getPrice());
        final Location5 warp = island.getWarp();
        if (warp == null) {
            bean.setWarpWorld(null);
            bean.setWarpX(null);
            bean.setWarpY(null);
            bean.setWarpZ(null);
            bean.setWarpYaw(null);
            bean.setWarpPitch(null);
        } else {
            final World warpWorld = warp.getWorld();
            if (warpWorld == null) {
                bean.setWarpWorld(null);
            } else {
                bean.setWarpWorld(warpWorld.getName());
            }
            bean.setWarpX(warp.getX());
            bean.setWarpY(warp.getY());
            bean.setWarpZ(warp.getZ());
            bean.setWarpYaw(warp.getYaw());
            bean.setWarpPitch(warp.getPitch());
        }
        return bean;
    }

    @Override
    public Object getId(final IslandBean bean) {
        return bean.getLocation();
    }
}
