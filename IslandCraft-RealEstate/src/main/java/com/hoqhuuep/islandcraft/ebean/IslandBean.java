package com.hoqhuuep.islandcraft.ebean;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "island")
public class IslandBean {
    @EmbeddedId
    private LocationBean location;
    @Column
    private String name;
    @Column
    private UUID ownerUUID;
    @Column
    private Double price;
    @Column
    private String warpWorld;
    @Column
    private Double warpX;
    @Column
    private Double warpY;
    @Column
    private Double warpZ;
    @Column
    private Float warpYaw;
    @Column
    private Float warpPitch;

    public LocationBean getLocation() {
        return location;
    }

    public void setLocation(final LocationBean location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public UUID getOwnerUUID() {
        return ownerUUID;
    }

    public void setOwnerUUID(final UUID owner) {
        this.ownerUUID = owner;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(final Double price) {
        this.price = price;
    }

    public String getWarpWorld() {
        return warpWorld;
    }

    public void setWarpWorld(final String warpWorld) {
        this.warpWorld = warpWorld;
    }

    public Double getWarpX() {
        return warpX;
    }

    public void setWarpX(final Double warpX) {
        this.warpX = warpX;
    }

    public Double getWarpY() {
        return warpY;
    }

    public void setWarpY(final Double warpY) {
        this.warpY = warpY;
    }

    public Double getWarpZ() {
        return warpZ;
    }

    public void setWarpZ(final Double warpZ) {
        this.warpZ = warpZ;
    }

    public Float getWarpYaw() {
        return warpYaw;
    }

    public void setWarpYaw(final Float warpYaw) {
        this.warpYaw = warpYaw;
    }

    public Float getWarpPitch() {
        return warpPitch;
    }

    public void setWarpPitch(final Float warpPitch) {
        this.warpPitch = warpPitch;
    }
}
