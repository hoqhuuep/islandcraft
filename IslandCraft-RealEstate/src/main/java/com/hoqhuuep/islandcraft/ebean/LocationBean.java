package com.hoqhuuep.islandcraft.ebean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LocationBean implements Serializable {
    private static final long serialVersionUID = -3606536401712949013L;
    @Column
    private String world;
    @Column
    private Integer x;
    @Column
    private Integer z;

    public String getWorld() {
        return world;
    }

    public void setWorld(final String world) {
        this.world = world;
    }

    public Integer getX() {
        return x;
    }

    public void setX(final Integer x) {
        this.x = x;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(final Integer z) {
        this.z = z;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((world == null) ? 0 : world.hashCode());
        result = prime * result + ((x == null) ? 0 : x.hashCode());
        result = prime * result + ((z == null) ? 0 : z.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (!(object instanceof LocationBean)) {
            return false;
        }
        final LocationBean that = (LocationBean) object;
        if (world == null) {
            if (that.world != null) {
                return false;
            }
        } else if (!world.equals(that.world)) {
            return false;
        }
        if (x == null) {
            if (that.x != null) {
                return false;
            }
        } else if (!x.equals(that.x)) {
            return false;
        }
        if (z == null) {
            if (that.z != null) {
                return false;
            }
        } else if (!z.equals(that.z)) {
            return false;
        }
        return true;
    }
}
