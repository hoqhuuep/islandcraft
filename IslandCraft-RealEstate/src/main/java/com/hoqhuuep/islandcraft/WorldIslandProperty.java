package com.hoqhuuep.islandcraft;

import org.apache.commons.lang.ObjectUtils;

import com.hoqhuuep.minecraft.World;
import com.hoqhuuep.util.property.Property;

public class WorldIslandProperty extends Property<Island> {
    private final World world;

    public WorldIslandProperty(final World world) {
        this.world = world;
    }

    public final World getWorld() {
        return world;
    }

    @Override
    public final boolean isSatisfiedBy(final Island island) {
        return ObjectUtils.equals(island.getLocation().getWorld(), world);
    }
}
