package com.hoqhuuep.islandcraft;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.hoqhuuep.islandcraft.config.WorldConfig;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.World;

public final class IslandCraftWorld {
    private final int islandGap;
    private final int islandSize;
    private final int islandSeparation;
    private final int innerRadius;
    private final int outerRadius;
    private final int magicNumber;
    private final int borderRadiusSquared;
    private final boolean useBorder;
    private final double resourceIslandRarity;

    public IslandCraftWorld(final WorldConfig config) {
        islandSize = config.ISLAND_SIZE;
        islandSeparation = config.ISLAND_SEPARATION;
        resourceIslandRarity = config.RESOURCE_ISLAND_RARITY;
        borderRadiusSquared = config.BORDER_RADIUS_SQUARED;
        useBorder = config.USE_BORDER;
        islandGap = islandSeparation - islandSize;
        innerRadius = islandSize / 2;
        outerRadius = innerRadius + islandGap;
        magicNumber = (islandSize - islandGap) / 2;
    }

    public boolean outsideBorder(final Location id) {
        final double centerX = id.getX();
        final double centerZ = id.getZ();
        final double minX = centerX - outerRadius;
        final double maxX = centerX + outerRadius;
        final double minZ = centerZ - outerRadius;
        final double maxZ = centerZ + outerRadius;
        return outsideBorder(minX, minZ) || outsideBorder(maxX, maxZ) || outsideBorder(minX, maxZ) || outsideBorder(maxX, minZ);
    }

    private boolean outsideBorder(final double x, final double z) {
        return useBorder && (x * x + z * z > borderRadiusSquared);
    }

    public Location getInnerIsland(final Location location) {
        if (location == null) {
            return null;
        }
        final int offsetZ = location.getZ() + islandSize / 2;
        final int relativeZ = ifloormod(offsetZ, islandSeparation);
        if (relativeZ >= islandSize) {
            return null;
        }
        final int evenOffsetX = location.getX() + islandSize / 2;
        final int row = ifloordiv(offsetZ, islandSeparation);
        final int offsetX;
        if (0 == row % 2) {
            offsetX = evenOffsetX;
        } else {
            offsetX = evenOffsetX + islandSeparation / 2;
        }
        final int relativeX = ifloormod(offsetX, islandSeparation);
        if (relativeX >= islandSize) {
            return null;
        }
        final int centerZ = row * islandSeparation;
        final int col = ifloordiv(offsetX, islandSeparation);
        final int centerX;
        if (0 == row % 2) {
            centerX = col * islandSeparation;
        } else {
            centerX = col * islandSeparation - islandSeparation / 2;
        }
        final World world = location.getWorld();
        return new Location(world, centerX, centerZ);
    }

    // Numbers represent how many island regions a location overlaps.
    // Arrows point towards the centers of the overlapped regions.
    // @-------+-----------+-------+-----------+
    // |...^...|.....^.....|..\./..|.....^.....|
    // |...3...|.....2.....|...3...|.....2.....|
    // |../.\..|.....v.....|...v...|.....v.....|
    // +-------+-----------+-------+-----------+
    // |.......|...............................|
    // |.......|...............................|
    // |.......|...............................|
    // |.......|...............................|
    // |.......|...............................|
    // |.......|...............................|
    // |..<2>..|...............#...............|
    // |.......|...............................|
    // |.......|...............................|
    // |.......|...............................|
    // |.......|...............................|
    // |.......|...............................|
    // |.......|...............................|
    // +-------+-----------+-------+-----------+
    // |..\./..|.....^.....|...^...|.....^.....|
    // |...3...|.....2.....|...3...|.....2.....|
    // |...v...|.....v.....|../.\..|.....v.....|
    // +-------+-----------+-------+-----------+
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...1...............|..<2>..|.......1>..|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // |...................|.......|...........|
    // +-------------------+-------+-----------+
    public List<Location> getOuterIslands(final Location location) {
        if (location == null) {
            return Collections.emptyList();
        }
        final World world = location.getWorld();
        final int x = location.getX();
        final int z = location.getZ();
        final int regionPatternXSize = outerRadius + innerRadius;
        final int regionPatternZSize = regionPatternXSize * 2;
        // # relative to @
        final int relativeHashX = outerRadius;
        final int relativeHashZ = outerRadius;
        // @ relative to world origin
        final int absoluteAtX = ifloordiv(x + relativeHashX, regionPatternXSize) * regionPatternXSize - relativeHashX;
        final int absoluteAtZ = ifloordiv(z + relativeHashZ, regionPatternZSize) * regionPatternZSize - relativeHashZ;
        // # relative to world origin
        final int absoluteHashX = absoluteAtX + relativeHashX;
        final int absoluteHashZ = absoluteAtZ + relativeHashZ;
        // Point to test relative to @
        final int relativeX = x - absoluteAtX;
        final int relativeZ = z - absoluteAtZ;
        final List<Location> result = new ArrayList<Location>();
        // Top
        if (relativeZ < islandGap) {
            final int centerZ = absoluteHashZ - islandSeparation;
            // Left
            if (relativeX < magicNumber + islandGap * 2) {
                final int centerX = absoluteHashX - islandSeparation / 2;
                final Location island = new Location(world, centerX, centerZ);
                result.add(island);
            }
            // Right
            if (relativeX >= magicNumber + islandGap) {
                final int centerX = absoluteHashX + islandSeparation / 2;
                final Location island = new Location(world, centerX, centerZ);
                result.add(island);
            }
        }
        // Middle
        if (relativeZ < outerRadius * 2) {
            // Left
            if (relativeX < islandGap) {
                final int centerX = absoluteHashX - islandSeparation;
                final Location island = new Location(world, centerX, absoluteHashZ);
                result.add(island);
            }
            // Right
            final Location island = new Location(world, absoluteHashX, absoluteHashZ);
            result.add(island);
        }
        // Bottom
        if (relativeZ >= islandSize + islandGap) {
            final int centerZ = absoluteHashZ + islandSeparation;
            // Left
            if (relativeX < magicNumber + islandGap * 2) {
                final int centerX = absoluteHashX - islandSeparation / 2;
                final Location island = new Location(world, centerX, centerZ);
                result.add(island);
            }
            // Right
            if (relativeX >= magicNumber + islandGap) {
                final int centerX = absoluteHashX + islandSeparation / 2;
                final Location island = new Location(world, centerX, centerZ);
                result.add(island);
            }
        }
        return result;
    }

    public boolean isResource(final Location island) {
        final long worldSeed = island.getWorld().getSeed();
        final int x = island.getX();
        final int z = island.getZ();
        return random(x, z, worldSeed) < resourceIslandRarity;
    }

    private double random(final int x, final int z, final long worldSeed) {
        // Random only uses lower 48 bits of seed
        final long islandSeed = worldSeed ^ (((long) z << 24) | x & 0x00FFFFFFL);
        final Random random = new Random(islandSeed);
        return random.nextDouble();
    }

    public final int getInnerRadius() {
        return innerRadius;
    }

    public final int getOuterRadius() {
        return outerRadius;
    }

    private static int ifloordiv(int n, int d) {
        // Credit to Mark Dickinson
        // http://stackoverflow.com/a/10466453
        if (d >= 0) {
            return n >= 0 ? n / d : ~(~n / d);
        } else {
            return n <= 0 ? n / d : (n - 1) / d - 1;
        }
    }

    private static int ifloormod(int n, int d) {
        // Credit to Mark Dickinson
        // http://stackoverflow.com/a/10466453
        if (d >= 0) {
            return n >= 0 ? n % d : d + ~(~n % d);
        } else {
            return n <= 0 ? n % d : d + 1 + (n - 1) % d;
        }
    }
}
