package com.hoqhuuep.islandcraft.config;

import org.bukkit.configuration.ConfigurationSection;

public class WorldConfig {
    public final boolean USE_BORDER;
    public final int BORDER_RADIUS_SQUARED;
    public final int ISLAND_SIZE;
    public final int ISLAND_SEPARATION;
    public final int ISLAND_GAP;
    public final double RESOURCE_ISLAND_RARITY;

    public WorldConfig(final ConfigurationSection config) throws ConfigException {
        ISLAND_SIZE = config.getInt("island-size");
        ISLAND_GAP = config.getInt("island-gap");
        ISLAND_SEPARATION = ISLAND_SIZE + ISLAND_GAP;
        final int maxIslands = config.getInt("max-islands");
        USE_BORDER = maxIslands >= 0;
        if (USE_BORDER) {
            final double radius = Math.sqrt(maxIslands * ISLAND_SEPARATION * ISLAND_SEPARATION / Math.PI) + Math.sqrt(2) * (ISLAND_SEPARATION - ISLAND_SIZE / 2);
            BORDER_RADIUS_SQUARED = (int) Math.ceil(radius * radius);
        } else {
            BORDER_RADIUS_SQUARED = 0;
        }
        // Validate configuration values
        if (ISLAND_SIZE <= 0 || ISLAND_SIZE % 32 != 0) {
            throw new ConfigException(config.getCurrentPath() + ".island-size must be a positive multiple of 32");
        }
        if (ISLAND_GAP <= 0 || ISLAND_GAP % 32 != 0) {
            throw new ConfigException(config.getCurrentPath() + ".island-gap must be a positive multiple of 32");
        }
        RESOURCE_ISLAND_RARITY = config.getDouble("resource-island-rarity");
        // Validate configuration values
        if (RESOURCE_ISLAND_RARITY < 0.0 || RESOURCE_ISLAND_RARITY > 1.0) {
            throw new ConfigException(config.getCurrentPath() + ".resource-island-rarity must be between 0.0 and 1.0");
        }
    }
}
