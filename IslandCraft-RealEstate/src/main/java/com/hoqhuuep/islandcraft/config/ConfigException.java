package com.hoqhuuep.islandcraft.config;

public class ConfigException extends Exception {
    private static final long serialVersionUID = -156293896223203258L;

    public ConfigException(final String message) {
        super(message);
    }
}
