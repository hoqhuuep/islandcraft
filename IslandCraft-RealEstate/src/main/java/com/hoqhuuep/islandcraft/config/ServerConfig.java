package com.hoqhuuep.islandcraft.config;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;

public class ServerConfig {
    public final double DEFAULT_PRICE;
    public final int MAX_ISLANDS_PER_PLAYER;
    public final Map<String, WorldConfig> WORLDS;

    public ServerConfig(final ConfigurationSection config) throws ConfigException {
        DEFAULT_PRICE = config.getDouble("default-price");
        MAX_ISLANDS_PER_PLAYER = config.getInt("max-islands-per-player");
        // Validate configuration values
        if (DEFAULT_PRICE < 0) {
            throw new ConfigException(config.getCurrentPath() + ".default-price must not be negative");
        }
        if (MAX_ISLANDS_PER_PLAYER < -1) {
            throw new ConfigException(config.getCurrentPath() + ".max-islands-per-player must not be less than -1");
        }
        WORLDS = new HashMap<String, WorldConfig>();
        final ConfigurationSection worlds = config.getConfigurationSection("worlds");
        for (final String key : worlds.getKeys(false)) {
            WORLDS.put(key, new WorldConfig(worlds.getConfigurationSection(key)));
        }
    }
}
