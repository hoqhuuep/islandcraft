package com.hoqhuuep.islandcraft;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.hoqhuuep.islandcraft.config.ServerConfig;
import com.hoqhuuep.islandcraft.config.WorldConfig;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.Location5;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.World;
import com.hoqhuuep.minecraft.chat.Chat;
import com.hoqhuuep.minecraft.command.CannotDoException;
import com.hoqhuuep.minecraft.economy.Economy;
import com.hoqhuuep.util.repository.Repository;

public class EstateAgent {
    private final Economy economy;
    private final Chat chat;
    private final Repository<Island> repository;
    private final ServerConfig config;
    private final Map<Player, Island> lastIsland;
    private final Map<String, IslandCraftWorld> islandCraftWorlds;
    private final Set<Location> loadedIslands;
    private final Phrases phrases;
    private final IslandObserver observer;

    public EstateAgent(final Repository<Island> repository, final ServerConfig config, final Economy economy, final Chat chat, final Phrases phrases, final IslandObserver observer) {
        this.repository = repository;
        this.config = config;
        this.economy = economy;
        this.chat = chat;
        this.phrases = phrases;
        this.observer = observer;
        lastIsland = Maps.newHashMap();
        islandCraftWorlds = Maps.newHashMap();
        loadedIslands = Sets.newHashSet();
    }

    public void buyIsland(final Player player, final Island island) throws CannotDoException {
        final OfflinePlayer owner = island.getOwner();
        if (player.equals(owner)) {
            throw new CannotDoException(phrases.errorAlreadyOwner());
        }
        final Double price = island.getPrice();
        if (price == null) {
            throw new CannotDoException(phrases.errorNotForSale());
        }
        // config.MAX_ISLANDS_PER_PLAYER < 0 => infinite
        if (config.MAX_ISLANDS_PER_PLAYER >= 0 && islandCount(player) >= config.MAX_ISLANDS_PER_PLAYER) {
            throw new CannotDoException(phrases.errorMaxIslands());
        }
        final World world = island.getLocation().getWorld();
        if (!economy.transfer(player, owner, world, island.getPrice())) {
            throw new CannotDoException(phrases.errorTransferFailed());
        }
        island.setName(null);
        island.setOwner(player);
        island.setPrice(null);
        updateIsland(island);
    }

    public void abandonIsland(final Player player, final Island island) throws CannotDoException {
        ensureOwner(island, player);
        island.setName(null);
        island.setOwner(null);
        island.setPrice(config.DEFAULT_PRICE);
        updateIsland(island);
    }

    public void sellIsland(final Player player, final Island island, final Double price) throws CannotDoException {
        ensureOwner(island, player);
        island.setPrice(price);
        updateIsland(island);
    }

    public final void renameIsland(final Player player, final Island island, final String name) throws CannotDoException {
        ensureOwner(island, player);
        island.setName(name);
        updateIsland(island);
    }

    public final void examineIsland(final Player player, final Island island) throws CannotDoException {
        chat.send(player, phrases.islandInfo(getDescription(island)));
    }

    public void listIslands(final Player player) throws CannotDoException {
        final List<Island> islands = repository.load(IslandProperty.forOwner(player));
        if (islands.isEmpty()) {
            throw new CannotDoException(phrases.errorNoIslands());
        }
        int index = 1;
        for (final Island island : islands) {
            chat.send(player, phrases.islandList(index++, getDescription(island)));
        }
    }

    public void setIslandWarp(final Player player, final Island island, final Location5 warp) throws CannotDoException {
        ensureOwner(island, player);
        island.setWarp(warp);
        updateIsland(island);
    }

    public void warpToIsland(final Player player, final String uuidOrName, final int index) throws CannotDoException {
        final OfflinePlayer owner = getOfflinePlayer(player.getServer(), uuidOrName);
        final List<Island> islands = repository.load(IslandProperty.forOwner(owner));
        if (index > islands.size()) {
            throw new CannotDoException(phrases.errorNoSuchIsland());
        }
        final Island island = islands.get(index - 1);
        final Location5 warp = island.getWarp();
        if (warp == null) {
            player.teleport(defaultWarp(island));
        } else {
            player.teleport(warp);
        }
        chat.send(player, phrases.success());
    }

    public void regenerateIsland(final Island island) throws CannotDoException {
        final Location location = island.getLocation();
        final int centerX = location.getX();
        final int centerZ = location.getZ();
        final World world = location.getWorld();
        final WorldConfig worldConfig = config.WORLDS.get(world.getName());
        final int radius = worldConfig.ISLAND_SIZE / 2 + worldConfig.ISLAND_GAP;
        final int minX = (centerX - radius) / BLOCKS_PER_CHUNK;
        final int minZ = (centerZ - radius) / BLOCKS_PER_CHUNK;
        final int maxX = (centerX + radius) / BLOCKS_PER_CHUNK;
        final int maxZ = (centerZ + radius) / BLOCKS_PER_CHUNK;
        // Must loop from high to low for trees to generate correctly
        for (int x = maxX - 1; x >= minX; --x) {
            for (int z = maxZ - 1; z >= minZ; --z) {
                // TODO queue these?
                world.regenerateChunk(x, z);
            }
        }
    }

    public void overrideIslandPrice(final Island island, final Double price) throws CannotDoException {
        island.setPrice(price);
        updateIsland(island);
    }

    public void overrideIslandName(final Island island, final String name) throws CannotDoException {
        island.setName(name);
        updateIsland(island);
    }

    public void overrideIslandOwner(final Island island, final String uuidOrName) throws CannotDoException {
        final OfflinePlayer owner = getOfflinePlayer(island.getLocation().getWorld().getServer(), uuidOrName);
        island.setOwner(owner);
        updateIsland(island);
    }

    public void overrideIslandWarp(final Island island, final Location5 warp) throws CannotDoException {
        island.setWarp(warp);
        updateIsland(island);
    }

    public void onLoad(final Location location) {
        final World world = location.getWorld();
        if (world == null) {
            // Not ready
            return;
        }
        final IslandCraftWorld islandCraftWorld = islandCraftWorlds.get(world.getName());
        if (islandCraftWorld == null) {
            // Not an IslandCraft world
            return;
        }
        for (final Location id : islandCraftWorld.getOuterIslands(location)) {
            if (loadedIslands.contains(id)) {
                // Only load once, until server is rebooted
                continue;
            }
            final List<Island> islands = repository.load(IslandProperty.forLocation(id));
            if (islands.isEmpty()) {
                final Island island;
                if (islandCraftWorld.isResource(id)) {
                    island = new Island(id, null, null, null, null);
                } else {
                    island = new Island(id, null, null, config.DEFAULT_PRICE, null);
                }
                updateIsland(island);
            }
            loadedIslands.add(id);
        }
    }

    private static final int BLOCKS_PER_CHUNK = 16;

    public void onMove(final Player player, final Location to) {
        if (to == null) {
            lastIsland.remove(player);
            return;
        }
        final IslandCraftWorld islandCraftWorld = islandCraftWorlds.get(to.getWorld().getName());
        final Island toIsland;
        if (islandCraftWorld != null) {
            final Location toIslandLocation = islandCraftWorld.getInnerIsland(to);
            if (toIslandLocation != null) {
                final List<Island> islands = repository.load(IslandProperty.forLocation(toIslandLocation));
                if (!islands.isEmpty()) {
                    toIsland = islands.get(0);
                } else {
                    toIsland = null;
                }
            } else {
                toIsland = null;
            }
        } else {
            toIsland = null;
        }
        final Island fromIsland = lastIsland.get(player);
        if (fromIsland != null) {
            final String fromDescription = getDescription(fromIsland);
            if (toIsland == null || !getDescription(toIsland).equals(fromDescription)) {
                chat.send(player, phrases.islandLeave(fromDescription));
            }
        }
        if (toIsland != null) {
            final String toDescription = getDescription(toIsland);
            if (fromIsland == null || !getDescription(fromIsland).equals(toDescription)) {
                chat.send(player, phrases.islandEnter(toDescription));
            }
            // Copy island so we can notice when it updates
            lastIsland.put(player, new Island(toIsland));
        } else {
            lastIsland.remove(player);
        }
    }

    public String getDescription(final Island island) {
        final String name = island.getName();
        final OfflinePlayer owner = island.getOwner();
        final Double price = island.getPrice();
        final String nameOrDefault;
        if (price == null && owner == null) {
            nameOrDefault = (name == null ? phrases.reservedIsland() : name);
        } else {
            nameOrDefault = (name == null ? phrases.untitledIsland() : name);
        }
        final String ownerName = owner == null ? null : owner.getName();
        final String priceString = price == null ? null : economy.format(price);
        if (priceString == null) {
            if (ownerName == null) {
                return phrases.description(nameOrDefault);
            }
            return phrases.descriptionWithOwner(nameOrDefault, ownerName);
        }
        if (ownerName == null) {
            return phrases.descriptionWithPrice(nameOrDefault, priceString);
        }
        return phrases.descriptionWithOwnerAndPrice(nameOrDefault, ownerName, priceString);
    }

    public void initWorld(final String worldName) {
        final WorldConfig worldConfig = config.WORLDS.get(worldName);
        final IslandCraftWorld islandCraftWorld = new IslandCraftWorld(worldConfig);
        islandCraftWorlds.put(worldName, islandCraftWorld);
    }

    public IslandCraftWorld getIslandCraftWorld(final World world) {
        return islandCraftWorlds.get(world.getName());
    }

    public Island islandAt(final Location location) throws CannotDoException {
        final IslandCraftWorld islandCraftWorld = getIslandCraftWorld(location.getWorld());
        if (islandCraftWorld == null) {
            throw new CannotDoException(phrases.errorNotInIslandCraftWorld());
        }
        final Location islandLocation = islandCraftWorld.getInnerIsland(location);
        if (islandLocation == null) {
            throw new CannotDoException(phrases.errorNotOnIsland());
        }
        final List<Island> islands = repository.load(IslandProperty.forLocation(islandLocation));
        if (islands.isEmpty()) {
            throw new CannotDoException(phrases.errorNotOnIsland());
        }
        return islands.get(0);
    }

    private void ensureOwner(final Island island, final Player player) throws CannotDoException {
        if (!player.equals(island.getOwner())) {
            throw new CannotDoException(phrases.errorNotOwner());
        }
    }

    private void updateIsland(final Island island) {
        repository.save(island);
        for (final Player witness : lastIsland.keySet()) {
            if (island.equals(lastIsland.get(witness))) {
                onMove(witness, witness.getLocation());
            }
        }
        observer.onUpdate(island, this);
    }

    private int islandCount(final Player player) {
        final List<Island> islands = repository.load(IslandProperty.forOwner(player));
        return islands.size();
    }

    private final Location5 defaultWarp(final Island island) {
        final Location location = island.getLocation();
        final World world = location.getWorld();
        final int x = location.getX();
        final int z = location.getZ();
        final int y = world.getSurfaceY(x, z);
        return new Location5(world, x, y, z, 0, 0);
    }

    private final OfflinePlayer getOfflinePlayer(final Server server, final String uuidOrName) throws CannotDoException {
        try {
            // Try UUID first
            final UUID uuid = UUID.fromString(uuidOrName);
            return server.getOfflinePlayer(uuid);
        } catch (final IllegalArgumentException e) {
            // Then try name
            final OfflinePlayer offlinePlayer = server.getOfflinePlayer(uuidOrName);
            if (offlinePlayer == null) {
                throw new CannotDoException(phrases.errorUnknownPlayer());
            }
            return offlinePlayer;
        }
    }
}
