package com.hoqhuuep.islandcraft;

import org.apache.commons.lang.ObjectUtils;

import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.util.property.Property;

public class OwnerIslandProperty extends Property<Island> {
    private final OfflinePlayer owner;

    public OwnerIslandProperty(final OfflinePlayer owner) {
        this.owner = owner;
    }

    public final OfflinePlayer getOwner() {
        return owner;
    }

    @Override
    public final boolean isSatisfiedBy(final Island island) {
        return ObjectUtils.equals(island.getOwner(), owner);
    }
}
