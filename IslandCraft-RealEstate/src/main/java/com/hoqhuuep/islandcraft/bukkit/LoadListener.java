package com.hoqhuuep.islandcraft.bukkit;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.WorldLoadEvent;

import com.hoqhuuep.islandcraft.EstateAgent;
import com.hoqhuuep.islandcraft.config.ServerConfig;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.bukkit.BukkitFactory;

public class LoadListener implements Listener {
    private static final int BLOCKS_PER_CHUNK = 16;
    private final EstateAgent realEstateManager;
    private final ServerConfig config;

    public LoadListener(final EstateAgent realEstateManager, final ServerConfig config) {
        this.realEstateManager = realEstateManager;
        this.config = config;
    }

    @EventHandler
    public final void onChunkLoad(final ChunkLoadEvent event) {
        chunkLoad(event.getChunk());
    }

    @EventHandler
    public final void onWorldLoad(final WorldLoadEvent event) {
        final World world = event.getWorld();
        final String worldName = world.getName();
        if (config.WORLDS.containsKey(worldName)) {
            realEstateManager.initWorld(worldName);
            for (final Chunk chunk : world.getLoadedChunks()) {
                chunkLoad(chunk);
            }
        }
    }

    private void chunkLoad(final Chunk chunk) {
        final int x = chunk.getX() * BLOCKS_PER_CHUNK;
        final int z = chunk.getZ() * BLOCKS_PER_CHUNK;
        final Location location = new Location(BukkitFactory.fromBukkitWorld(chunk.getWorld()), x, z);
        realEstateManager.onLoad(location);
    }
}
