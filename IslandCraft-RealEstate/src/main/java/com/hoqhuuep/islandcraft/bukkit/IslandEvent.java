package com.hoqhuuep.islandcraft.bukkit;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.hoqhuuep.islandcraft.Island;
import com.hoqhuuep.islandcraft.EstateAgent;

public class IslandEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final Island island;
    private final EstateAgent realEstateManager;

    public IslandEvent(final Island island, final EstateAgent realEstateManager) {
        this.island = island;
        this.realEstateManager = realEstateManager;
    }

    public Island getIsland() {
        return island;
    }

    public EstateAgent getRealEstateManager() {
        return realEstateManager;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
