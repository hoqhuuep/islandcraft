package com.hoqhuuep.islandcraft.bukkit;

import org.bukkit.plugin.PluginManager;

import com.hoqhuuep.islandcraft.Island;
import com.hoqhuuep.islandcraft.IslandObserver;
import com.hoqhuuep.islandcraft.EstateAgent;

public class BukkitIslandObserver implements IslandObserver {
    private final PluginManager pluginManager;

    public BukkitIslandObserver(final PluginManager pluginManager) {
        this.pluginManager = pluginManager;
    }

    @Override
    public void onUpdate(final Island island, final EstateAgent realEstateManager) {
        pluginManager.callEvent(new IslandEvent(island, realEstateManager));
    }
}
