package com.hoqhuuep.islandcraft.bukkit;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.PersistenceException;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.ImmutableList;
import com.hoqhuuep.islandcraft.Island;
import com.hoqhuuep.islandcraft.IslandCommand;
import com.hoqhuuep.islandcraft.IslandObserver;
import com.hoqhuuep.islandcraft.Phrases;
import com.hoqhuuep.islandcraft.EstateAgent;
import com.hoqhuuep.islandcraft.config.ConfigException;
import com.hoqhuuep.islandcraft.config.ServerConfig;
import com.hoqhuuep.islandcraft.ebean.IslandBean;
import com.hoqhuuep.islandcraft.ebean.IslandEbeanRepository;
import com.hoqhuuep.islandcraft.ebean.LocationBean;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.bukkit.BukkitChat;
import com.hoqhuuep.minecraft.bukkit.BukkitCommandExecutor;
import com.hoqhuuep.minecraft.bukkit.BukkitFactory;
import com.hoqhuuep.minecraft.chat.Chat;
import com.hoqhuuep.minecraft.command.Command;
import com.hoqhuuep.minecraft.command.CommandHandler;
import com.hoqhuuep.minecraft.economy.Economy;
import com.hoqhuuep.minecraft.economy.vault.VaultEconomy;
import com.hoqhuuep.util.locale.ResourceBundleLocalizer;
import com.hoqhuuep.util.repository.Repository;

public final class ICRealEstatePlugin extends JavaPlugin {
    private static final Logger log = Logger.getLogger("Minecraft");

    public void onEnable() {
        final String[] files = { "messages_en.properties" };
        for (String name : files) {
            final File file = new File(getDataFolder(), name);
            if (!file.exists()) {
                saveResource("messages_en.properties", false);
            }
        }
        final PluginManager pluginManager = getServer().getPluginManager();
        final Economy economy = getEconomy();
        final Repository<Island> islandRepository = getIslandRepository();
        final ServerConfig config = getServerConfig();
        final Phrases phrases = ResourceBundleLocalizer.getPhraseBundle(Phrases.class, getDataFolder());
        final IslandObserver observer = new BukkitIslandObserver(pluginManager);
        final Chat chat = new BukkitChat(getServer());
        // Manager
        final EstateAgent realEstateManager = new EstateAgent(islandRepository, config, economy, chat, phrases, observer);
        // Commands
        getCommand("island").setExecutor(new BukkitCommandExecutor(new CommandHandler(ImmutableList.<Command> of(new IslandCommand(realEstateManager, chat, phrases)), chat)));
        // Events
        pluginManager.registerEvents(new LoadListener(realEstateManager, config), this);
        pluginManager.registerEvents(new PlayerListener(realEstateManager), this);
    }

    private Economy getEconomy() {
        final RegisteredServiceProvider<net.milkbowl.vault.economy.Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider == null) {
            return null;
        }
        return new VaultEconomy(economyProvider.getProvider());
    }

    private Repository<Island> getIslandRepository() {
        // Hack to ensure database exists
        try {
            getDatabase().find(IslandBean.class).findRowCount();
        } catch (final PersistenceException e) {
            installDDL();
        }
        final Server server = BukkitFactory.fromBukkitServer(getServer());
        return new IslandEbeanRepository(getDatabase(), server);
    }

    private ServerConfig getServerConfig() {
        saveDefaultConfig();
        try {
            return new ServerConfig(getConfig());
        } catch (final ConfigException e) {
            log.severe(e.getMessage());
            return null;
        }
    }

    @Override
    public List<Class<?>> getDatabaseClasses() {
        final Class<?>[] classes = { IslandBean.class, LocationBean.class };
        return Arrays.asList(classes);
    }
}
