package com.hoqhuuep.islandcraft.bukkit;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import com.hoqhuuep.islandcraft.EstateAgent;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.bukkit.BukkitFactory;

public class PlayerListener implements Listener {
    private final EstateAgent realEstateManager;

    public PlayerListener(final EstateAgent realEstateManager) {
        this.realEstateManager = realEstateManager;
    }

    @EventHandler
    public void onPlayerMove(final PlayerMoveEvent event) {
        final Player player = BukkitFactory.fromBukkitPlayer(event.getPlayer());
        realEstateManager.onMove(player, player.getLocation());
    }

    @EventHandler
    public void onPlayerTeleport(final PlayerTeleportEvent event) {
        final Player player = BukkitFactory.fromBukkitPlayer(event.getPlayer());
        realEstateManager.onMove(player, player.getLocation());
    }

    @EventHandler
    public void onPlayerChangedWorld(final PlayerChangedWorldEvent event) {
        final Player player = BukkitFactory.fromBukkitPlayer(event.getPlayer());
        realEstateManager.onMove(player, player.getLocation());
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final Player player = BukkitFactory.fromBukkitPlayer(event.getPlayer());
        realEstateManager.onMove(player, player.getLocation());
    }

    @EventHandler
    public void onPlayerLogout(final PlayerQuitEvent event) {
        final Player player = BukkitFactory.fromBukkitPlayer(event.getPlayer());
        realEstateManager.onMove(player, null);
    }
}
