package com.hoqhuuep.islandcraft;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.File;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.hoqhuuep.islandcraft.config.ConfigException;
import com.hoqhuuep.islandcraft.config.ServerConfig;
import com.hoqhuuep.islandcraft.mock.MockEconomy;
import com.hoqhuuep.islandcraft.mock.MockIslandRepository;
import com.hoqhuuep.islandcraft.mock.MockPlayer;
import com.hoqhuuep.islandcraft.mock.MockServer;
import com.hoqhuuep.islandcraft.mock.MockWorld;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.Location5;
import com.hoqhuuep.minecraft.chat.Chat;
import com.hoqhuuep.minecraft.command.Command;
import com.hoqhuuep.minecraft.command.CommandException;
import com.hoqhuuep.minecraft.command.CommandHandler;
import com.hoqhuuep.util.locale.ResourceBundleLocalizer;

public class TestBuy {
    // Invoker
    private CommandHandler command;
    // Domain objects
    private MockServer server;
    private MockWorld worldIslandCraft;
    private MockWorld worldNether;
    private MockPlayer buyer;
    private MockPlayer seller;
    private Island island;
    // Passive systems
    private Phrases phrases;
    private ServerConfig config;
    // Active systems
    private MockEconomy economy;
    private MockIslandRepository repository;
    private Chat chat;
    private IslandObserver observer;

    @Before
    public void setUp() throws ConfigException, CommandException {
        economy = spy(new MockEconomy());
        repository = spy(new MockIslandRepository());
        observer = mock(IslandObserver.class);
        chat = mock(Chat.class);
        phrases = ResourceBundleLocalizer.getPhraseBundle(Phrases.class, new File("src/main/resources/messages"));
        final ConfigurationSection yaml = YamlConfiguration.loadConfiguration(new File("src/main/resources/config.yml"));
        config = new ServerConfig(yaml);
        server = new MockServer();
        worldIslandCraft = new MockWorld("world_islandcraft", server);
        worldNether = new MockWorld("world_nether", server);
        server.addWorld(worldIslandCraft);
        server.addWorld(worldNether);
        buyer = new MockPlayer("buyer", new Location5(worldIslandCraft, 0.0, 0.0, 0.0, 0.0f, 0.0f));
        seller = new MockPlayer("seller", new Location5(worldIslandCraft, 0.0, 0.0, 0.0, 0.0f, 0.0f));
        worldIslandCraft.addPlayer(buyer);
        worldIslandCraft.addPlayer(seller);
        buyer.addPermission("islandcraft.command.island.buy");
        economy.transfer(null, buyer, worldIslandCraft, 100.0);
        final EstateAgent estateAgent = new EstateAgent(repository, config, economy, chat, phrases, observer);
        estateAgent.initWorld("world_islandcraft");
        estateAgent.onLoad(new Location(worldIslandCraft, -160, -320));
        estateAgent.onLoad(new Location(worldIslandCraft, 160, -320));
        estateAgent.onLoad(new Location(worldIslandCraft, -320, 0));
        estateAgent.onLoad(new Location(worldIslandCraft, 0, 0));
        estateAgent.onLoad(new Location(worldIslandCraft, 320, 0));
        estateAgent.onLoad(new Location(worldIslandCraft, -160, 320));
        estateAgent.onLoad(new Location(worldIslandCraft, 160, 320));
        command = new CommandHandler(ImmutableList.<Command> of(new IslandCommand(estateAgent, chat, phrases)), chat);
        island = estateAgent.islandAt(buyer.getLocation());
        island.setOwner(seller);
        reset(economy, chat, observer, repository);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(economy, chat, observer);
        // TODO verify repository interaction
        // verifyNoMoreInteractions(repository);
    }

    @Test
    public void testSuccess() {
        command.execute(buyer, "island buy");
        assertEquals(island.getOwner(), buyer);
        assertEquals(island.getPrice(), null);
        verify(chat).send(buyer, phrases.success());
        verify(economy).transfer(buyer, seller, worldIslandCraft, config.DEFAULT_PRICE);
        verify(repository).save(island);
        verify(observer).onUpdate(any(Island.class), any(EstateAgent.class));
    }

    @Test
    public void testSuccess2() {
        // Buy from server (owner is null)
        island.setOwner(null);
        command.execute(buyer, "island buy");
        assertEquals(island.getOwner(), buyer);
        assertEquals(island.getPrice(), null);
        verify(chat).send(buyer, phrases.success());
        verify(economy).transfer(buyer, null, worldIslandCraft, config.DEFAULT_PRICE);
        verify(repository, times(1)).save(any(Island.class));
        verify(repository).save(island);
        verify(observer).onUpdate(any(Island.class), any(EstateAgent.class));
    }

    @Test
    public void testNotForSale() {
        // The island must be for sale (price must not be null)
        island.setPrice(null);
        command.execute(buyer, "island buy");
        verify(chat).send(buyer, phrases.errorNotForSale());
    }

    @Test
    public void testAlreadyOwner() {
        // Buyer must not be the seller
        island.setOwner(buyer);
        command.execute(buyer, "island buy");
        verify(chat).send(buyer, phrases.errorAlreadyOwner());
    }

    @Test
    public void testAlreadyOwner2() {
        // Buyer must not be the seller
        command.execute(buyer, "island buy");
        command.execute(buyer, "island buy");
        verify(chat).send(buyer, phrases.success());
        verify(chat).send(buyer, phrases.errorAlreadyOwner());
        verify(economy).transfer(buyer, seller, worldIslandCraft, config.DEFAULT_PRICE);
        verify(observer).onUpdate(any(Island.class), any(EstateAgent.class));
    }

    @Test
    public void testNotOnIsland() {
        // Buyer must be standing on island
        buyer.teleport(new Location5(worldIslandCraft, 160.0, 0.0, 0.0, 0.0f, 0.0f));
        command.execute(buyer, "island buy");
        verify(chat).send(buyer, phrases.errorNotOnIsland());
    }

    @Test
    public void testNoPermission() {
        // Buyer must have islandcraft.command.island.buy permission
        buyer.removePermission("islandcraft.command.island.buy");
        command.execute(buyer, "island buy");
        // TODO add permission to phrases
        verify(chat).send(buyer, "You do not have permission to use that command");
    }

    @Test
    public void testInsufficientMoney() {
        // Buyer must have sufficient money for payment
        economy.transfer(buyer, null, worldIslandCraft, 100.0);
        command.execute(buyer, "island buy");
        verify(economy).transfer(buyer, null, worldIslandCraft, 100.0);
        verify(economy).transfer(buyer, seller, worldIslandCraft, config.DEFAULT_PRICE);
        // TODO individual messages for withdraw and deposit
        verify(chat).send(buyer, phrases.errorTransferFailed());
    }

    @Test
    public void testInsufficientSpace() {
        // Seller must have sufficient space for payment
        economy.transfer(null, seller, worldIslandCraft, 1000.0);
        command.execute(buyer, "island buy");
        verify(economy).transfer(null, seller, worldIslandCraft, 1000.0);
        verify(economy).transfer(buyer, seller, worldIslandCraft, config.DEFAULT_PRICE);
        // TODO individual messages for withdraw and deposit
        verify(chat).send(buyer, phrases.errorTransferFailed());
    }

    @Test
    public void testSellerNotOnline() {
        // TODO Seller must be online OR economy plugin must support offline deposits
        chat.send(buyer, "TODO");
        verify(chat).send(buyer, "TODO");
    }

    @Test
    public void testWrongWorld() {
        // Buyer must be in an IslandCraft enabled world
        buyer.teleport(new Location5(worldNether, 0.0, 0.0, 0.0, 0.0f, 0.0f));
        worldIslandCraft.removePlayer(buyer);
        worldNether.addPlayer(buyer);
        command.execute(buyer, "island buy");
        verify(chat).send(buyer, phrases.errorNotInIslandCraftWorld());
    }

    @Test
    public void testMaxIslands() {
        // Can only buy up to 3 islands
        buyer.teleport(new Location5(worldIslandCraft, 0.0, 0.0, 0.0, 0.0f, 0.0f));
        command.execute(buyer, "island buy");
        buyer.teleport(new Location5(worldIslandCraft, 320.0, 0.0, 0.0, 0.0f, 0.0f));
        command.execute(buyer, "island buy");
        buyer.teleport(new Location5(worldIslandCraft, 160.0, 0.0, 320.0, 0.0f, 0.0f));
        command.execute(buyer, "island buy");
        buyer.teleport(new Location5(worldIslandCraft, -160.0, 0.0, -320.0, 0.0f, 0.0f));
        command.execute(buyer, "island buy");
        verify(chat, times(3)).send(buyer, phrases.success());
        verify(chat).send(buyer, phrases.errorMaxIslands());
        verify(economy).transfer(buyer, seller, worldIslandCraft, config.DEFAULT_PRICE);
        verify(economy, times(2)).transfer(buyer, null, worldIslandCraft, config.DEFAULT_PRICE);
        verify(observer, times(3)).onUpdate(any(Island.class), any(EstateAgent.class));
    }

    @Test
    public void testTooManyParameters() {
        // Command must be /island buy
        command.execute(buyer, "island buy 123");
        // TODO localize message
        verify(chat).send(buyer, "�c/island buy");
    }

    @Test
    public void testConsoleSender() {
        // Command must be /island buy
        command.execute(server, "island buy");
        // TODO localize message
        verify(chat).send(server, "�cYou must be a player to use that command");
    }

    @Test
    public void testOtherSender() {
        // Command must be /island buy
        command.execute(null, "island buy");
        // TODO localize message
        verify(chat).send(null, "�cYou cannot do that");
    }
}
