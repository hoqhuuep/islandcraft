package com.hoqhuuep.islandcraft.mock;

import java.util.Map;

import org.apache.commons.lang.Validate;

import com.google.common.collect.Maps;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.World;
import com.hoqhuuep.minecraft.economy.Economy;

public class MockEconomy implements Economy {
    private final Map<World, Map<OfflinePlayer, Double>> accounts;
    public static final double minBalance = 0.0;
    public static final double maxBalance = 1000.0;

    public MockEconomy() {
        accounts = Maps.newHashMap();
    }

    private boolean withdraw(final OfflinePlayer player, final World world, final double amount) {
        final Map<OfflinePlayer, Double> worldAccounts = accounts.get(world);
        if (worldAccounts == null) {
            return false;
        }
        final Double balance = worldAccounts.get(player);
        if (balance == null || balance - amount < minBalance) {
            return false;
        }
        worldAccounts.put(player, balance - amount);
        return true;
    }

    private boolean deposit(final OfflinePlayer player, final World world, final double amount) {
        Map<OfflinePlayer, Double> worldAccounts = accounts.get(world);
        if (worldAccounts == null) {
            worldAccounts = Maps.newHashMap();
            worldAccounts.put(player, amount);
            accounts.put(world, worldAccounts);
            return true;
        }
        Double balance = worldAccounts.get(player);
        if (balance == null) {
            if (amount > maxBalance) {
                return false;
            }
            worldAccounts.put(player, amount);
            return true;
        }
        if (balance + amount > maxBalance) {
            return false;
        }
        worldAccounts.put(player, balance + amount);
        return true;
    }

    @Override
    public boolean transfer(final OfflinePlayer from, final OfflinePlayer to, final World world, final double amount) {
        System.out.println("  Economy.transfer(from: " + from + ", to: " + to + ", amount: " + amount + ")");
        Validate.notNull(world);
        if (from != null && !withdraw(from, world, amount)) {
            System.out.println("  TRANSFER FAILED");
            return false;
        }
        if (to != null && !deposit(to, world, amount)) {
            // Undo withdraw, should never fail
            if (from != null) {
                deposit(from, world, amount);
            }
            System.out.println("  TRANSFER FAILED");
            return false;
        }
        // Success
        return true;
    }

    @Override
    public String format(final double amount) {
        return String.format("$%.2f", amount);
    }
}
