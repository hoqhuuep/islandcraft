package com.hoqhuuep.islandcraft.mock;

import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.World;

public class MockWorld implements World {
    private final Server server;
    private final String name;
    private final UUID uid;
    private final List<Player> players;

    public MockWorld(final String name, final Server server) {
        this.name = name;
        uid = UUID.nameUUIDFromBytes(name.getBytes());
        this.server = server;
        players = Lists.newArrayList();
    }

    public void addPlayer(final Player player) {
        players.add(player);
    }

    public void removePlayer(final Player player) {
        players.remove(player);
    }

    @Override
    public UUID getUID() {
        return uid;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public long getSeed() {
        return 0;
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public List<Player> getPlayers() {
        return Lists.newArrayList(players);
    }

    @Override
    public void regenerateChunk(final int x, final int z) {
        System.out.println("  REGENERATE");
    }

    @Override
    public int getSurfaceY(final int x, final int z) {
        return 64;
    }

    @Override
    public String toString() {
        return "MockWorld(uid: " + uid + ", name: " + name + ")";
    }
}
