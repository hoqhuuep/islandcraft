package com.hoqhuuep.islandcraft.mock;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hoqhuuep.islandcraft.Island;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.util.repository.Repository;

public class MockIslandRepository extends Repository<Island> {
    private final Map<Location, Island> islands;

    public MockIslandRepository() {
        islands = Maps.newHashMap();
    }

    @Override
    public void save(final Island thing) {
        islands.put(thing.getLocation(), thing);
        System.out.println("  Repository.save(" + thing + ")");
    }

    @Override
    public List<Island> loadAll() {
        return Lists.newArrayList(islands.values());
    }
}
