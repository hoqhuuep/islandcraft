package com.hoqhuuep.islandcraft.mock;

import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.Location5;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.World;

public class MockPlayer implements Player {
    private final String name;
    private final UUID uuid;
    private Location5 location;
    private List<String> permissions;

    public MockPlayer(final String name, final Location5 location) {
        this.name = name;
        uuid = UUID.nameUUIDFromBytes(name.getBytes());
        this.location = location;
        permissions = Lists.newArrayList();
    }

    public void addPermission(final String permission) {
        permissions.add(permission);
    }

    public void removePermission(final String permission) {
        permissions.remove(permission);
    }

    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Player getPlayer() {
        return this;
    }

    @Override
    public boolean hasPermission(final String permission) {
        return permissions.contains(permission);
    }

    @Override
    public Server getServer() {
        return getWorld().getServer();
    }

    @Override
    public Location getLocation() {
        return new Location(getWorld(), (int) Math.floor(location.getX()), (int) Math.floor(location.getZ()));
    }

    @Override
    public Location5 getLocation5() {
        return location;
    }

    @Override
    public World getWorld() {
        return location.getWorld();
    }

    @Override
    public void teleport(final Location5 warp) {
        location = warp;
    }

    @Override
    public String toString() {
        return "MockPlayer(uuid: " + uuid + ", name: " + name + ")";
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof OfflinePlayer) {
            OfflinePlayer player = (OfflinePlayer) object;
            return getUUID().equals(player.getUUID());
        }
        return false;
    }

    @Override
    public void sendMessage(final String message) {
    }
}
