package com.hoqhuuep.islandcraft.mock;

import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;
import com.hoqhuuep.minecraft.OfflinePlayer;
import com.hoqhuuep.minecraft.Player;
import com.hoqhuuep.minecraft.Server;
import com.hoqhuuep.minecraft.World;

public class MockServer implements Server {
    private final List<World> worlds;

    public MockServer() {
        worlds = Lists.newArrayList();
    }

    public void addWorld(final World world) {
        worlds.add(world);
    }

    @Override
    public boolean hasPermission(final String permission) {
        return true;
    }

    @Override
    public Server getServer() {
        return this;
    }

    @Override
    public Player getPlayer(final UUID uuid) {
        for (final World world : worlds) {
            for (final Player player : world.getPlayers()) {
                if (uuid.equals(player.getUUID())) {
                    return player;
                }
            }
        }
        return null;
    }

    @Override
    public Player getPlayer(final String name) {
        for (final World world : worlds) {
            for (final Player player : world.getPlayers()) {
                if (name.equals(player.getName())) {
                    return player;
                }
            }
        }
        return null;
    }

    @Override
    public OfflinePlayer getOfflinePlayer(final UUID uuid) {
        return getPlayer(uuid);
    }

    @Override
    public OfflinePlayer getOfflinePlayer(final String name) {
        return getPlayer(name);
    }

    @Override
    public World getWorld(final UUID uid) {
        for (final World world : worlds) {
            if (uid.equals(world.getUID())) {
                return world;
            }
        }
        return null;
    }

    @Override
    public World getWorld(String name) {
        for (final World world : worlds) {
            if (name.equals(world.getName())) {
                return world;
            }
        }
        return null;
    }

    @Override
    public List<World> getWorlds() {
        return Lists.newArrayList(worlds);
    }

    @Override
    public List<Player> getPlayers() {
        final List<Player> players = Lists.newArrayList();
        for (final World world : worlds) {
            players.addAll(world.getPlayers());
        }
        return players;
    }

    @Override
    public List<OfflinePlayer> getOfflinePlayers() {
        final List<Player> players = getPlayers();
        final List<OfflinePlayer> offlinePlayers = Lists.newArrayListWithCapacity(players.size());
        offlinePlayers.addAll(players);
        return offlinePlayers;
    }

    @Override
    public void sendMessage(final String message) {
    }
}
