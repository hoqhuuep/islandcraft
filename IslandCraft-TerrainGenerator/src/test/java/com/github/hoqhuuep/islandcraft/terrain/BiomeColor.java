package com.github.hoqhuuep.islandcraft.terrain;

import java.awt.Color;

import org.bukkit.block.Biome;

public class BiomeColor {
    public static final Color[] fromId = new Color[256];
    static {
        fromId[Biome.BEACH.ordinal()] = new Color(250, 222, 85);
        fromId[Biome.BIRCH_FOREST.ordinal()] = new Color(48, 116, 68);
        fromId[Biome.BIRCH_FOREST_HILLS.ordinal()] = new Color(31, 95, 50);
        fromId[Biome.BIRCH_FOREST_HILLS_MOUNTAINS.ordinal()] = new Color(71, 135, 90);
        fromId[Biome.BIRCH_FOREST_MOUNTAINS.ordinal()] = new Color(88, 156, 108);
        fromId[Biome.COLD_BEACH.ordinal()] = new Color(250, 240, 192);
        fromId[Biome.COLD_TAIGA.ordinal()] = new Color(49, 85, 74);
        fromId[Biome.COLD_TAIGA_HILLS.ordinal()] = new Color(36, 63, 54);
        fromId[Biome.COLD_TAIGA_MOUNTAINS.ordinal()] = new Color(89, 125, 114);
        fromId[Biome.DEEP_OCEAN.ordinal()] = new Color(0, 0, 48);
        fromId[Biome.DESERT.ordinal()] = new Color(250, 148, 24);
        fromId[Biome.DESERT_HILLS.ordinal()] = new Color(210, 95, 18);
        fromId[Biome.DESERT_MOUNTAINS.ordinal()] = new Color(255, 188, 64);
        fromId[Biome.EXTREME_HILLS.ordinal()] = new Color(96, 96, 96);
        fromId[Biome.SMALL_MOUNTAINS.ordinal()] = new Color(114, 120, 154);
        fromId[Biome.EXTREME_HILLS_MOUNTAINS.ordinal()] = new Color(136, 136, 136);
        fromId[Biome.EXTREME_HILLS_PLUS.ordinal()] = new Color(80, 112, 80);
        fromId[Biome.EXTREME_HILLS_PLUS_MOUNTAINS.ordinal()] = new Color(120, 152, 120);
        fromId[Biome.FLOWER_FOREST.ordinal()] = new Color(45, 142, 73);
        fromId[Biome.FOREST.ordinal()] = new Color(5, 102, 33);
        fromId[Biome.FOREST_HILLS.ordinal()] = new Color(34, 85, 28);
        fromId[Biome.FROZEN_OCEAN.ordinal()] = new Color(144, 144, 160);
        fromId[Biome.FROZEN_RIVER.ordinal()] = new Color(160, 160, 255);
        fromId[Biome.HELL.ordinal()] = new Color(255, 0, 0);
        fromId[Biome.ICE_MOUNTAINS.ordinal()] = new Color(160, 160, 160);
        fromId[Biome.ICE_PLAINS.ordinal()] = new Color(255, 255, 255);
        fromId[Biome.ICE_PLAINS_SPIKES.ordinal()] = new Color(180, 220, 220);
        fromId[Biome.JUNGLE.ordinal()] = new Color(83, 123, 9);
        fromId[Biome.JUNGLE_EDGE.ordinal()] = new Color(98, 139, 23);
        fromId[Biome.JUNGLE_EDGE_MOUNTAINS.ordinal()] = new Color(138, 179, 63);
        fromId[Biome.JUNGLE_HILLS.ordinal()] = new Color(44, 66, 5);
        fromId[Biome.JUNGLE_MOUNTAINS.ordinal()] = new Color(123, 163, 49);
        fromId[Biome.MEGA_SPRUCE_TAIGA.ordinal()] = new Color(129, 142, 121);
        fromId[Biome.MEGA_SPRUCE_TAIGA_HILLS.ordinal()] = new Color(109, 119, 102);
        fromId[Biome.MEGA_TAIGA.ordinal()] = new Color(89, 102, 81);
        fromId[Biome.MEGA_TAIGA_HILLS.ordinal()] = new Color(69, 79, 62);
        fromId[Biome.MESA.ordinal()] = new Color(217, 69, 21);
        fromId[Biome.MESA_BRYCE.ordinal()] = new Color(255, 109, 61);
        fromId[Biome.MESA_PLATEAU.ordinal()] = new Color(202, 140, 101);
        fromId[Biome.MESA_PLATEAU_FOREST.ordinal()] = new Color(176, 151, 101);
        fromId[Biome.MESA_PLATEAU_FOREST_MOUNTAINS.ordinal()] = new Color(216, 191, 141);
        fromId[Biome.MESA_PLATEAU_MOUNTAINS.ordinal()] = new Color(242, 180, 141);
        fromId[Biome.MUSHROOM_ISLAND.ordinal()] = new Color(255, 0, 255);
        fromId[Biome.MUSHROOM_SHORE.ordinal()] = new Color(160, 0, 255);
        fromId[Biome.OCEAN.ordinal()] = new Color(0, 0, 112);
        fromId[Biome.PLAINS.ordinal()] = new Color(141, 179, 96);
        fromId[Biome.RIVER.ordinal()] = new Color(0, 0, 255);
        fromId[Biome.ROOFED_FOREST.ordinal()] = new Color(64, 81, 26);
        fromId[Biome.ROOFED_FOREST_MOUNTAINS.ordinal()] = new Color(104, 121, 66);
        fromId[Biome.SAVANNA.ordinal()] = new Color(189, 178, 95);
        fromId[Biome.SAVANNA_MOUNTAINS.ordinal()] = new Color(229, 218, 135);
        fromId[Biome.SAVANNA_PLATEAU.ordinal()] = new Color(167, 157, 100);
        fromId[Biome.SAVANNA_PLATEAU_MOUNTAINS.ordinal()] = new Color(207, 197, 140);
        fromId[Biome.SKY.ordinal()] = new Color(128, 128, 255);
        fromId[Biome.STONE_BEACH.ordinal()] = new Color(162, 162, 132);
        fromId[Biome.SUNFLOWER_PLAINS.ordinal()] = new Color(181, 219, 136);
        fromId[Biome.SWAMPLAND.ordinal()] = new Color(7, 249, 178);
        fromId[Biome.SWAMPLAND_MOUNTAINS.ordinal()] = new Color(47, 255, 218);
        fromId[Biome.TAIGA.ordinal()] = new Color(11, 102, 89);
        fromId[Biome.TAIGA_HILLS.ordinal()] = new Color(22, 57, 51);
        fromId[Biome.TAIGA_MOUNTAINS.ordinal()] = new Color(51, 142, 129);
    }

    private BiomeColor() {
        // Utility class
    }
}
