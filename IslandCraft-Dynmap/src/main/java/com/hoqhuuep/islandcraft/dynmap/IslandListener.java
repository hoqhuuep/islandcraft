package com.hoqhuuep.islandcraft.dynmap;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.dynmap.markers.AreaMarker;
import org.dynmap.markers.MarkerSet;

import com.hoqhuuep.islandcraft.Island;
import com.hoqhuuep.islandcraft.EstateAgent;
import com.hoqhuuep.islandcraft.bukkit.IslandEvent;
import com.hoqhuuep.minecraft.Location;
import com.hoqhuuep.minecraft.World;

public class IslandListener implements Listener {
    final MarkerSet markerSet;
    final DynmapConfig config;

    public IslandListener(final MarkerSet markerSet, final DynmapConfig config) {
        this.markerSet = markerSet;
        this.config = config;
    }

    @EventHandler
    public void onIsland(final IslandEvent event) {
        final Island island = event.getIsland();
        final EstateAgent realEstateManager = event.getRealEstateManager();
        final Location location = island.getLocation();
        final World world = location.getWorld();
        final String worldName = world.getName();
        final int x = location.getX();
        final int z = location.getZ();
        final String markerId = worldName + "'" + x + "'" + z;
        final int radius = realEstateManager.getIslandCraftWorld(world).getInnerRadius();
        final double[] xs = { x - radius, x - radius, x + radius, x + radius };
        final double[] zs = { z - radius, z + radius, z + radius, z - radius };
        AreaMarker areaMarker = markerSet.findAreaMarker(markerId);
        if (areaMarker == null) {
            areaMarker = markerSet.createAreaMarker(markerId, realEstateManager.getDescription(island), false, worldName, xs, zs, true);
        } else {
            areaMarker.setLabel(realEstateManager.getDescription(island), false);
        }
        final AreaConfig areaConfig;
        areaMarker.setDescription(realEstateManager.getDescription(island));
        areaConfig = config.RESOURCE;
        areaMarker.setFillStyle(areaConfig.FILL_OPACITY, areaConfig.FILL_COLOR);
        areaMarker.setLineStyle(areaConfig.LINE_WIDTH, areaConfig.LINE_OPACITY, areaConfig.LINE_COLOR);
    }
}
